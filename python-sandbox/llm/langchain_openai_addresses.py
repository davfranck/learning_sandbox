import os
import openai
from langchain.prompts import ChatPromptTemplate
from dotenv import load_dotenv, find_dotenv
from langchain.chat_models import ChatOpenAI
from langchain.output_parsers import ResponseSchema
from langchain.output_parsers import StructuredOutputParser


_ = load_dotenv(find_dotenv()) # read local .env file
openai.api_key = os.environ['OPENAI_API_KEY']

chat = ChatOpenAI(temperature=0.0, model="gpt-3.5-turbo")

addresses = """
1 rue de la paix, 75000 Paris;
24 avenue du général Buat, 44200 Nantes;
17 boulevard du compagnon, 01000 Trifouilli les oies;
"""

street_number_schema = ResponseSchema(name="street_number", 
                                      description="The number of the street if provided. If no street number is found, then set null.")

street_schema = ResponseSchema(name="street", 
                                      description="The street without the street number.")

zip_code_schema = ResponseSchema(name="zip_code", 
                                 description="The zip code of the city.")

city_schema = ResponseSchema(name="city", description="the city extracted from the address.")

response_schemas = [street_number_schema, street_schema, zip_code_schema, city_schema]

output_parser = StructuredOutputParser.from_response_schemas(response_schemas)
format_instructions = output_parser.get_format_instructions()

review_template_2 = """\
For the following list of addresses, separated with ";", extract the following information:

street_number : extract the number of the street. 
street : extract the street without the number
zip_code : extract the zip_code
city : extract the city

text: {addresses}

{format_instructions}
"""
prompt = ChatPromptTemplate.from_template(template=review_template_2)

messages = prompt.format_messages(text=addresses, 
                                format_instructions=format_instructions)

response = chat(messages)
print(response.content)

output_dict = output_parser.parse(response.content)
print(output_dict)

