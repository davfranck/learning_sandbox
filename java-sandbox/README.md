# Java sandbox

Je mets ici toutes les expérimentations utiles à ma compréhension sur tout ce qui concerne java. 
Si j'ai des besoins spécifiques non compatibles, je tire une branche. 

liste des branches : 
* main - Java 18
  * JPA
    * embeddable
    * onetoone mapping
    * dto (projections)
    * `@EntityGraph`
  * Spring cloud open feign
  * CompletableFuture
  * encoding
  * moneta
  * nombres réels
  * textblocks
  * CQRS : from the conference https://www.youtube.com/watch?v=qBLtZN3p3FU (approximately)
    * TODO : le retour des queries c'est pas top
    * TODO : voir pour les pbs de types générics
    * TODO : validations

Libs et frameworks :
* [api](https://docs.oracle.com/en/java/javase/18/docs/api/index.html)
* [assertj](https://assertj.github.io/doc/)
* [spring boot](https://docs.spring.io/spring-boot/docs/3.0.0/reference/htmlsingle/) - 3.0.0
* [spring cloud OpenFeign](https://cloud.spring.io/spring-cloud-openfeign/reference/html/)
* [moneta](https://github.com/JavaMoney/jsr354-ri)