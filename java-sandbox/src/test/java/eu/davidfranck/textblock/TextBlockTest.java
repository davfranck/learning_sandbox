package eu.davidfranck.textblock;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TextBlockTest {

    @Test
    void textBlock_indentation() {
        String textBlock = """
                Hello David,
                How are you ?
                  two spaces before
                    one tab before
                back to the beginning
                """;
        String[] textBlockSplitted = textBlock.split(System.lineSeparator());
        assertThat(textBlockSplitted[0]).isEqualTo("Hello David,");
        assertThat(textBlockSplitted[1]).isEqualTo("How are you ?");
        assertThat(textBlockSplitted[2]).isEqualTo("  two spaces before");
        assertThat(textBlockSplitted[3]).isEqualTo("    one tab before");
        assertThat(textBlockSplitted[4]).isEqualTo("back to the beginning");
    }

    @Test
    void identation_depends_on_the_last_three_quotes() {
        String textBlock = """
                <hello>
                    <world/>
                </hello>
             """;
        String[] textBlockSplitted = textBlock.split(System.lineSeparator());
        assertThat(textBlockSplitted[0]).isEqualTo("   <hello>");
        assertThat(textBlockSplitted[1]).isEqualTo("       <world/>");
        assertThat(textBlockSplitted[2]).isEqualTo("   </hello>");
    }

    @Test
    void whitespaces_to_the_right_are_stripped_unless_forced() {
        String textBlock = """
                One space after removed 
                One space forced after\s
                """;
        String[] textBlockSplitted = textBlock.split(System.lineSeparator());
        assertThat(textBlockSplitted[0]).isEqualTo("One space after removed");
        assertThat(textBlockSplitted[1]).isEqualTo("One space forced after ");
    }

    @Test
    void double_quotes_dont_have_to_be_escaped() {
        String textBlock = """
                Hello "bob"
                Special \"""case\""" with triple quotes
                """;
        String[] textBlockSplitted = textBlock.split(System.lineSeparator());
        assertThat(textBlockSplitted[0]).isEqualTo("Hello \"bob\"");
        assertThat(textBlockSplitted[1]).isEqualTo("Special \"\"\"case\"\"\" with triple quotes");
    }

    @Test
    void we_can_add_a_new_line_for_better_readability_and_keep_it_at_the_same_time_as_one_line() {
        String textBlock = """
                Hello \
                David
                """;
        assertThat(textBlock).isEqualTo("Hello David" + System.lineSeparator());
    }

    @Test
    void formatted_method() {
        String textBlock = """
                Hello 
                %s
                How are you ?
                """.formatted("David");
        String[] split = textBlock.split(System.lineSeparator());
        assertThat(split[1]).isEqualTo("David");
    }
}
