package eu.davidfranck.completablefuture;

import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class CompletableFutureTest {
    @Test
    void oneCompletableFuture() {
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> longMethod(1));
        assertThat(completableFuture.join()).isEqualTo(33888890);
    }

    @Test
    void manyCallsToLongMethodWithoutFuture() {
        int sum = longMethod(1) + longMethod(2) + longMethod(3) + longMethod(4);
        assertThat(sum).isEqualTo(135555560);
    }

    @Test
    void manyCallsToLongMethodWithFutureIsFaster() {
        CompletableFuture<Integer> first = CompletableFuture.supplyAsync(() -> longMethod(1));
        CompletableFuture<Integer> second = CompletableFuture.supplyAsync(() -> longMethod(2));
        CompletableFuture<Integer> third = CompletableFuture.supplyAsync(() -> longMethod(3));
        CompletableFuture<Integer> fourth = CompletableFuture.supplyAsync(() -> longMethod(4));
        Integer sum = CompletableFuture
                .allOf(first,
                       second,
                       third,
                       fourth)
                .thenApply(unused -> first.join() + second.join() + third.join() + fourth.join())
                .join();
        assertThat(sum).isEqualTo(135555560);
    }

    @Test
    void handleException() {
        CompletableFuture<Integer> first = CompletableFuture.supplyAsync(() -> longMethod(1));
        CompletableFuture<Integer> second = CompletableFuture.supplyAsync(() -> errorMethod(2));
        CompletableFuture<Integer> third = CompletableFuture.supplyAsync(() -> longMethod(3));
        CompletableFuture<Integer> fourth = CompletableFuture.supplyAsync(() -> errorMethod(4));
        Throwable throwable = catchThrowable(() -> CompletableFuture
                .allOf(first,
                       second,
                       third,
                       fourth)
                .thenAccept(unused -> System.out.println("not called"))
                .join());
        assertThat(throwable)
                .as("the exception is thrown during allOff.join call")
                .isInstanceOf(RuntimeException.class)
                .hasMessage("java.lang.IllegalArgumentException: KO2");
    }

    private Integer errorMethod(int callNumber) {
        System.out.printf("Call N° %s - error\n",
                          callNumber);
        throw new IllegalArgumentException("KO" + callNumber);
    }

    private int longMethod(int callNumber) {
        System.out.printf("Call N° %s - long\n",
                          callNumber);
        return IntStream
                .range(0,
                       5_000_000)
                .mapToObj(Integer::toString)
                .mapToInt(String::length)
                .sum();
    }
}
