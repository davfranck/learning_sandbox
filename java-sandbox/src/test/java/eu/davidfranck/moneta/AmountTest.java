package eu.davidfranck.moneta;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Locale;
import javax.money.Monetary;
import org.javamoney.moneta.FastMoney;
import org.junit.jupiter.api.Test;

class AmountTest {

  @Test
  void name() {
    final FastMoney eightyDollars = FastMoney.of(80, Monetary.getCurrency(Locale.US));
    assertThat(eightyDollars.toString()).isEqualTo("USD 80.00");
  }
}
