package eu.davidfranck.moneta;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Locale;
import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.format.AmountFormatQueryBuilder;
import javax.money.format.MonetaryAmountFormat;
import javax.money.format.MonetaryFormats;
import org.javamoney.moneta.Money;
import org.javamoney.moneta.format.CurrencyStyle;
import org.junit.jupiter.api.Test;

public class FormatAmountTest {

  static final CurrencyUnit DOLLAR = Monetary.getCurrency(Locale.US);

  @Test
  void name() {
    final Money amount = Money.of(21345.12D, DOLLAR);
    final MonetaryAmountFormat franceFormat = MonetaryFormats.getAmountFormat(Locale.FRANCE);
    final MonetaryAmountFormat usFormat = MonetaryFormats.getAmountFormat(Locale.US);
    final MonetaryAmountFormat customFormat = MonetaryFormats.getAmountFormat(
        AmountFormatQueryBuilder.of(Locale.US).set(CurrencyStyle.SYMBOL).build());
    assertThat(franceFormat.format(amount)).isEqualTo("21 345,12 USD");
    assertThat(usFormat.format(amount)).isEqualTo("USD21,345.12");
    assertThat(customFormat.format(amount)).isEqualTo("$21,345.12");
  }
}
