package eu.davidfranck.moneta;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Locale;
import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import org.javamoney.moneta.Money;
import org.javamoney.moneta.function.MonetaryFunctions;
import org.junit.jupiter.api.Test;

class StreamOperations {

  static final CurrencyUnit DOLLAR = Monetary.getCurrency(Locale.US);
  static final List<MonetaryAmount> MONETARY_AMOUNTS = List.of(Money.of(10, DOLLAR), Money.of(33,
          DOLLAR),
      Money.of(13, DOLLAR));

  @Test
  void max() {
    assertThat(MONETARY_AMOUNTS.stream().reduce(MonetaryFunctions.max()).get()).isEqualTo(
        Money.of(33, DOLLAR));
  }

  @Test
  void min() {
    assertThat(MONETARY_AMOUNTS.stream().reduce(MonetaryFunctions.min()).get()).isEqualTo(
        Money.of(10, DOLLAR));
  }

  @Test
  void sum() {
    assertThat(MONETARY_AMOUNTS.stream().reduce(MonetaryFunctions.sum()).get()).isEqualTo(
        Money.of(56, DOLLAR));
  }
}
