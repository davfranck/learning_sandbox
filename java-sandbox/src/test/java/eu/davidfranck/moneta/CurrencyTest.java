package eu.davidfranck.moneta;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Locale;
import javax.money.Monetary;
import org.junit.jupiter.api.Test;

public class CurrencyTest {

  @Test
  void create_currency() {
    assertThat(Monetary.getCurrency(Locale.US).getCurrencyCode()).isEqualTo("USD");
    assertThat(Monetary.getCurrency(Locale.FRANCE).getCurrencyCode()).isEqualTo("EUR");
    assertThat(Monetary.getCurrency("BRL").getCurrencyCode()).isEqualTo("BRL");
  }
}
