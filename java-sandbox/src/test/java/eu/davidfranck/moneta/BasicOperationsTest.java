package eu.davidfranck.moneta;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Locale;
import javax.money.CurrencyUnit;
import javax.money.Monetary;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

public class BasicOperationsTest {

  final CurrencyUnit euro = Monetary.getCurrency(Locale.FRANCE);
  final Money oneHundredTwentyEuros = Money.of(120, euro);
  final Money fiftyEuros = Money.of(50, euro);

  @Test
  void operations() {
    assertThat(oneHundredTwentyEuros.add(fiftyEuros)).isEqualTo(Money.of(170, euro));
    assertThat(oneHundredTwentyEuros.subtract(fiftyEuros)).isEqualTo(Money.of(70, euro));
    // on divise par un nombre, long double ou Number, pas par un Money/MoneytaryAmount.
    assertThat(oneHundredTwentyEuros.divide(2)).isEqualTo(Money.of(60, euro));
  }

  @Test
  void comparisons() {
    assertThat(oneHundredTwentyEuros.isGreaterThan(fiftyEuros)).isTrue();
  }
}
