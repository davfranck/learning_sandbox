package eu.davidfranck.feign;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.jsonResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.*;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

@SpringBootTest
@WireMockTest(httpPort = 9099)
public class FooFeignTest {

  @Autowired
  private SampleFeignClient sampleFeignClient;

  @Test
  void deserializeWhenUnknownField() {
    String json = """
        { "aField": "aValue", "anotherField": "anotherValue", "unknownField": "unknownFieldValue" }
        """;
    WireMock.stubFor(get(urlEqualTo("/foo")).willReturn(jsonResponse(json, HttpStatus.OK.value())));
    FooFeign foo = sampleFeignClient.getFoo();
    assertThat(foo).isNotNull();
    assertThat(foo).extracting(FooFeign::aField, FooFeign::anotherField)
        .contains("aValue", "anotherValue");
  }
}
