package eu.davidfranck.cqrs.alemaire.subscription;

import eu.davidfranck.cqrs.alemaire.core.CommandResponse;
import eu.davidfranck.cqrs.alemaire.activity.domain.Activity;
import eu.davidfranck.cqrs.alemaire.activity.infra.ActivityJpaRepository;
import eu.davidfranck.cqrs.alemaire.subscription.command.CreateSubscriptionPlanCommand;
import eu.davidfranck.cqrs.alemaire.subscription.command.CreateSubscriptionPlanCommandHandler;
import eu.davidfranck.cqrs.alemaire.subscription.infra.SubscriptionJpaRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SubscriptionPlanCommandHandlerTest {
    @Autowired
    private CreateSubscriptionPlanCommandHandler createSubscriptionPlanCommandHandler;

    @Autowired
    private ActivityJpaRepository activityRepository;
    @Autowired
    private SubscriptionJpaRepository subscriptionJpaRepository;

    @BeforeEach
    public void setup() {
        activityRepository.save(new Activity(1L, "activity 1"));
        activityRepository.save(new Activity(2L, "activity 2"));
    }

    @AfterEach
    public void cleanUp(){
        subscriptionJpaRepository.deleteAll();
        activityRepository.deleteAll();
    }
    @Test
    void createSubscriptionPlan() {
        // given
        CreateSubscriptionPlanCommand command = new CreateSubscriptionPlanCommand(123L, "le plan", LocalDate.now(), 345L, List.of(1L));
        // when
        CommandResponse commandResponse = createSubscriptionPlanCommandHandler.handle(command);
        // then
        assertThat(commandResponse.id()).isNotNull();
    }
}
