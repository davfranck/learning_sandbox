package eu.davidfranck.jpa.version.unidirectional;

import eu.davidfranck.jpa.version.unidirectional.model.ChildVersion;
import eu.davidfranck.jpa.version.unidirectional.model.ParentVersion;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional(propagation = Propagation.NEVER)
public class UnidirectionalVersionTest {

    @Autowired
    private UnidirectionalVersionService unidirectionalVersionService;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PlatformTransactionManager transactionManager;
    private TransactionTemplate transactionTemplate;


    @BeforeEach
    public void setUp() {
        transactionTemplate = new TransactionTemplate(transactionManager);
    }

    @Test
    void shouldIncrementParentVersion() {
        Long idResult = transactionTemplate.execute(status -> {
            ParentVersion parentVersion = new ParentVersion("any value");
            unidirectionalVersionService.saveParent(parentVersion);
            return parentVersion.getId();
        });
        ParentVersion parentResult = getParentVersion(idResult);
        assertThat(parentResult.getVersion()).isNotNull();

        Long updatedId = transactionTemplate.execute(status -> {
            parentResult.setParentValue("change value");
            unidirectionalVersionService.saveParent(parentResult);
            return parentResult.getId();
        });
        ParentVersion updatedParent = getParentVersion(updatedId);
        assertThat(updatedParent.getVersion()).isEqualTo(1);
    }

    @Test
    void shouldNotIncrementParentVersionWhenRelationIsUnidirectionalAndOwnedByChild() {
        Long idResult = transactionTemplate.execute(status -> {
            ParentVersion parentVersion = new ParentVersion("any value");
            unidirectionalVersionService.saveParent(parentVersion);
            return parentVersion.getId();
        });
        ParentVersion parentResult = getParentVersion(idResult);
        assertThat(parentResult.getVersion()).isNotNull();

        Long childId = transactionTemplate.execute(status -> {
            ChildVersion child = new ChildVersion("Child", parentResult);
            unidirectionalVersionService.saveChild(child);
            return child.getId();
        });
        ParentVersion updatedParent = getParentVersion(idResult);
        assertThat(updatedParent.getVersion()).as("aucun impact sur la version").isEqualTo(0);
    }


    private ParentVersion getParentVersion(Long idResult) {
        ParentVersion parentResult = transactionTemplate.execute(status -> {
            return unidirectionalVersionService.getParent(idResult);
        });
        return parentResult;
    }
}
