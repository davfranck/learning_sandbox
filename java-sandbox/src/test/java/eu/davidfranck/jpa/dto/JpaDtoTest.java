package eu.davidfranck.jpa.dto;

import static org.assertj.core.api.Assertions.assertThat;

import eu.davidfranck.jpa.dto.dto.MusicianDto;
import eu.davidfranck.jpa.dto.model.Album;
import eu.davidfranck.jpa.dto.model.Musician;
import eu.davidfranck.jpa.dto.model.MusicianAddress;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class JpaDtoTest {

  @Autowired
  private MusicianRepository repository;

  @Test
  void retrieve_dto_with_interfaces() {
    Album album1 = new Album("titre");
    Album album2 = new Album("titre2");
    List<Album> albums = List.of(album1, album2);
    repository.save(
        new Musician(null, "Auteur 1", new MusicianAddress("rue au pif"), albums));

    List<MusicianDto> musicians = repository.findAllMusiciansDto();

    assertThat(musicians).isNotNull();
    assertThat(musicians).singleElement()
        .satisfies(musicianDto -> {
          assertThat(musicianDto.getName()).isEqualTo("Auteur 1");
          assertThat(musicianDto.getAlbums()).hasSize(2);
          assertThat(musicianDto.getAlbums().get(0).getTitle()).isEqualTo("titre");
        });

  }

}
