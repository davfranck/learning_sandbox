package eu.davidfranck.jpa.embeddable;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.ListAssert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.util.Map;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
public class EmbeddableTest {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AuthorRepository authorRepository;

    @AfterEach
    void afterEach() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "author");
    }

    @Test
    void embeddable() {
        Author author = new Author("important", new AuthorName("john", "doe"));
        authorRepository.save(author);
        ListAssert<Map<String, Object>> result = assertThat(jdbcTemplate.queryForList("select * from author"));
        result.singleElement()
                .satisfies(row -> assertThat(row).contains(
                        entry("something", "important"),
                        entry("first_name", "john"),
                        entry("family_name", "doe")));
    }
}
