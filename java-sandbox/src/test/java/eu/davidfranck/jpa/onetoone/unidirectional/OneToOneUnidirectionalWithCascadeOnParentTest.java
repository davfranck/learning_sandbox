package eu.davidfranck.jpa.onetoone.unidirectional;

import eu.davidfranck.jpa.onetoone.unidirectionnal.OneToOneUnidirectionalService;
import eu.davidfranck.jpa.onetoone.unidirectionnal.model.Address;
import eu.davidfranck.jpa.onetoone.unidirectionnal.model.FooUserWithAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.util.List;
import java.util.Map;

import static eu.davidfranck.jpa.onetoone.unidirectionnal.model.Address.AddressBuilder.createAddressBuilder;
import static eu.davidfranck.jpa.onetoone.unidirectionnal.model.FooUserWithAddress.FooUserWithAddressBuilder.createFooUserWithAddressBuilder;
import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * FooUserWithAddress has one Address : @OneToOne with Cascade.ALL.
 * The foreign key is in the FOO_USER_WITH_ADDRESS table.
 * The relation is unidirectional.
 */
@SpringBootTest
public class OneToOneUnidirectionalWithCascadeOnParentTest {

    @Autowired
    private OneToOneUnidirectionalService service;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @AfterEach
    public void afterEach() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "foo_user_with_address", "address");
    }

    @Test
    void create() {
        // given
        Address address = createAddressBuilder().withValue("ADDRESS").build();
        FooUserWithAddress user = createFooUserWithAddressBuilder().withName("FOO_USERNAME").withAddress(address).build();

        // when
        service.saveFoo(user);

        // then
        List<Map<String, Object>> addressResult = jdbcTemplate.queryForList("select * from address");
        assertThat(addressResult)
                .singleElement()
                .satisfies(addressRow -> assertThat(addressRow).containsAnyOf(entry("ZE_VALUE", "ADDRESS")));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "ADDRESS")).isEqualTo(1);

        List<Map<String, Object>> userResult = jdbcTemplate.queryForList("select * from foo_user_with_address");
        assertThat(userResult)
                .singleElement()
                .satisfies(fooUserRow -> assertThat(fooUserRow).containsAnyOf(entry("NAME", "FOO_USERNAME")));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "FOO_USER_WITH_ADDRESS")).isEqualTo(1);
    }

    /**
     * Generated queries at the "when" step :
     * <ul>
     * <li>select foouserwit0_.id as id1_2_1_, foouserwit0_.address_id as address_3_2_1_, foouserwit0_.name as name2_2_1_, address1_.id as id1_0_0_, address1_.ze_value as ze_value2_0_0_ from foo_user_with_address foouserwit0_ left outer join address address1_ on foouserwit0_.address_id=address1_.id where foouserwit0_.id=?</li>
     * <li>update address set ze_value=? where id=?</li>
     * <li>update foo_user_with_address set address_id=?, name=? where id=?</li>
     * </ul>
     * The select is triggered by the "merge" statement (called by the save method of the repository).
     */
    @Test
    void update_user_and_address() {
        // given
        Address address = createAddressBuilder().withValue("ADDRESS").build();
        FooUserWithAddress user = createFooUserWithAddressBuilder().withName("FOO_USERNAME").withAddress(address).build();
        service.saveFoo(user);

        // when
        service.saveFoo(createFooUserWithAddressBuilder()
                .withId(user.getId())
                .withName("UPDATED NAME")
                .withAddress(createAddressBuilder()
                        .withValue("UPDATED ADDRESS VALUE")
                        .withId(address.getId())
                        .build())
                .build());

        // then
        List<Map<String, Object>> addressResult = jdbcTemplate.queryForList("select * from address");
        assertThat(addressResult)
                .singleElement()
                .satisfies(addressRow -> assertThat(addressRow).containsAnyOf(entry("ZE_VALUE", "UPDATED ADDRESS VALUE")));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "ADDRESS")).isEqualTo(1);

        List<Map<String, Object>> userResult = jdbcTemplate.queryForList("select * from foo_user_with_address");
        assertThat(userResult)
                .singleElement()
                .satisfies(fooUserRow -> assertThat(fooUserRow).containsAnyOf(entry("NAME", "UPDATED NAME")));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "FOO_USER_WITH_ADDRESS")).isEqualTo(1);
    }

    /**
     * Generated queries at the "when" step :
     * <ul>
     *     <li>select address0_.id as id1_0_0_, address0_.ze_value as ze_value2_0_0_ from address address0_ where address0_.id=?</li>
     *     <li>update address set ze_value=? where id=?</li>
     * </ul>
     */
    @Test
    void update_address_only() {
        // given
        Address address = createAddressBuilder().withValue("ADDRESS").build();
        FooUserWithAddress user = createFooUserWithAddressBuilder().withName("FOO_USERNAME").withAddress(address).build();
        service.saveFoo(user);

        Address updatedAddress = createAddressBuilder()
                .withValue("UPDATED ADDRESS")
                .withId(address.getId())
                .build();

        // when
        service.saveAddress(updatedAddress);

        // then
        List<Map<String, Object>> addressResult = jdbcTemplate.queryForList("select * from address");
        assertThat(addressResult)
                .singleElement()
                .satisfies(addressRow -> assertThat(addressRow).containsAnyOf(entry("ZE_VALUE", "UPDATED ADDRESS")));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "ADDRESS")).isEqualTo(1);

        List<Map<String, Object>> userResult = jdbcTemplate.queryForList("select * from foo_user_with_address");
        assertThat(userResult)
                .singleElement()
                .satisfies(fooUserRow -> assertThat(fooUserRow).containsAnyOf(
                        entry("NAME", "FOO_USERNAME"),
                        entry("ADDRESS_ID", address.getId())));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "FOO_USER_WITH_ADDRESS")).isEqualTo(1);
    }
}
