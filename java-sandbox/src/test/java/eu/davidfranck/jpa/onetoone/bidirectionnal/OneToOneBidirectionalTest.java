package eu.davidfranck.jpa.onetoone.bidirectionnal;

import eu.davidfranck.jpa.onetoone.bidirectionnal.model.Phone;
import eu.davidfranck.jpa.onetoone.bidirectionnal.model.UserWithPhone;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.util.List;
import java.util.Map;

import static eu.davidfranck.jpa.onetoone.bidirectionnal.model.Phone.PhoneBuilder.createPhoneBuilder;
import static eu.davidfranck.jpa.onetoone.bidirectionnal.model.UserWithPhone.UserWithPhoneBuilder.createUserWithPhoneBuilder;
import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * UserWithPhone has a one to one relation with Phone.
 * Phone is the owner (= he has a foreign key user_with_phone_id).
 * UserWithPhone is the parent.
 */
@SpringBootTest
public class OneToOneBidirectionalTest {

    @Autowired
    private OneToOneBiDirectionalService service;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @AfterEach
    void afterEach() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "phone", "user_with_phone");
    }

    /**
     * Generated queries at the "when" step :
     * <ul>
     *     <li>insert into user_with_phone (name, id) values (?, ?)</li>
     *     <li>insert into phone (user_with_phone_id, ze_value, id) values (?, ?, ?)</li>
     * </ul>
     */
    @Test
    void create_user_and_phone() {
        // given
        Phone phone = createPhoneBuilder().withValue("007").build();
        UserWithPhone userWithPhone = createUserWithPhoneBuilder().withName("bob").build();
        userWithPhone.addPhone(phone);

        // when
        service.saveUserWithPhone(userWithPhone);

        // then
        assertPhoneResult("007", userWithPhone.getId());
        assertUserResult("bob", userWithPhone.getId());

    }

    @Test
    void create_user_then_create_phone() {
        // given
        UserWithPhone userWithPhone = createUserWithPhoneBuilder().withName("bob").build();
        service.saveUserWithPhone(userWithPhone);

        Phone phone = createPhoneBuilder()
                .withValue("001")
                .withUserWithPhone(createUserWithPhoneBuilder().withId(userWithPhone.getId()).build())
                .build();

        // when
        service.savePhone(phone);

        // then
        assertPhoneResult("001", userWithPhone.getId());
        assertUserResult("bob", userWithPhone.getId());
    }

    /**
     * https://vladmihalcea.com/spring-data-jpa-findbyid/
     * <p>We use here getReferenceById instead of creating an object manually by valuing the id</p>
     */
    @Test
    void create_user_then_create_phone_with_getByReference() {
        // given
        UserWithPhone userWithPhone = createUserWithPhoneBuilder().withName("bob").build();
        service.saveUserWithPhone(userWithPhone);

        Phone phone = createPhoneBuilder()
                .withValue("001")
                .build();

        // when
        service.savePhone(phone, userWithPhone.getId());

        // then
        assertPhoneResult("001", userWithPhone.getId());
        assertUserResult("bob", userWithPhone.getId());
    }

    /**
     * Generated queries at the "when" step :
     * <ul>>
     * <li>select userwithph0_.id as id1_4_1_, userwithph0_.name as name2_4_1_, phone1_.id as id1_3_0_, phone1_.user_with_phone_id as user_wit3_3_0_, phone1_.ze_value as ze_value2_3_0_ from user_with_phone userwithph0_ left outer join phone phone1_ on userwithph0_.id=phone1_.user_with_phone_id where userwithph0_.id=?</li>
     * <li>select phone0_.id as id1_3_0_, phone0_.user_with_phone_id as user_wit3_3_0_, phone0_.ze_value as ze_value2_3_0_ from phone phone0_ where phone0_.id=?</li>
     * <li>update user_with_phone set name=? where id=?</li>
     * <li>update phone set user_with_phone_id=?, ze_value=? where id=?</li>
     * </ul
     */
    @Test
    void update_user_and_phone() {
        // given
        Phone phone = createPhoneBuilder().withValue("007").build();
        UserWithPhone userWithPhone = createUserWithPhoneBuilder().withName("bob").build();
        userWithPhone.addPhone(phone);
        service.saveUserWithPhone(userWithPhone);

        Phone phoneToUpdate = createPhoneBuilder()
                .withValue("008")
                .withId(phone.getId())
                .build();
        UserWithPhone userToUpdate = createUserWithPhoneBuilder()
                .withName("bob update")
                .withId(userWithPhone.getId())
                .build();

        userToUpdate.addPhone(phoneToUpdate);// TODO - smell - ça limite l'intérêt du builder

        // when
        service.saveUserWithPhone(userToUpdate);

        // then
        assertPhoneResult("008", userWithPhone.getId());
        assertUserResult("bob update", userToUpdate.getId());

    }

    /**
     * Generated queries at the "when" step :
     * <p>
     * Hibernate: select phone0_.id as id1_3_0_, phone0_.user_with_phone_id as user_wit3_3_0_, phone0_.ze_value as ze_value2_3_0_ from phone phone0_ where phone0_.id=?
     * Hibernate: select userwithph0_.id as id1_4_1_, userwithph0_.name as name2_4_1_, phone1_.id as id1_3_0_, phone1_.user_with_phone_id as user_wit3_3_0_, phone1_.ze_value as ze_value2_3_0_ from user_with_phone userwithph0_ left outer join phone phone1_ on userwithph0_.id=phone1_.user_with_phone_id where userwithph0_.id=?
     * Hibernate: select phone0_.id as id1_3_1_, phone0_.user_with_phone_id as user_wit3_3_1_, phone0_.ze_value as ze_value2_3_1_, userwithph1_.id as id1_4_0_, userwithph1_.name as name2_4_0_ from phone phone0_ inner join user_with_phone userwithph1_ on phone0_.user_with_phone_id=userwithph1_.id where phone0_.user_with_phone_id=?
     * Hibernate: update phone set user_with_phone_id=?, ze_value=? where id=?
     */
    @Test
    void update_transient_phone_with_user_id() {
        // given
        Phone phone = createPhoneBuilder().withValue("007").build();
        UserWithPhone userWithPhone = createUserWithPhoneBuilder().withName("bob").build();
        userWithPhone.addPhone(phone);
        service.saveUserWithPhone(userWithPhone);

        Phone phoneToUpdate = createPhoneBuilder()
                .withValue("009")
                .withId(phone.getId())
                .withUserWithPhone(createUserWithPhoneBuilder().withId(userWithPhone.getId()).build())
                .build();

        // when
        service.savePhone(phoneToUpdate);

        // then
        assertPhoneResult("009", userWithPhone.getId());
        assertUserResult("bob", userWithPhone.getId());
    }

    private void assertPhoneResult(String value, Long userWithPhoneId) {
        List<Map<String, Object>> phoneResult = jdbcTemplate.queryForList("select * from phone");
        assertThat(phoneResult)
                .singleElement()
                .satisfies(phoneRow -> assertThat(phoneRow).contains(
                        entry("ZE_VALUE", value),
                        entry("user_with_phone_id", userWithPhoneId)));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "PHONE")).isEqualTo(1);
    }

    private void assertUserResult(String username, Long id) {
        List<Map<String, Object>> userResult = jdbcTemplate.queryForList("select * from user_with_phone");
        assertThat(userResult)
                .singleElement()
                .satisfies(barUserRow -> assertThat(barUserRow).contains(
                        entry("NAME", username),
                        entry("ID", id)));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "user_with_phone")).isEqualTo(1);
    }
}
