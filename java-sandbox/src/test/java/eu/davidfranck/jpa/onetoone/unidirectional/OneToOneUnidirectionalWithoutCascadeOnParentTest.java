package eu.davidfranck.jpa.onetoone.unidirectional;

import eu.davidfranck.jpa.onetoone.unidirectionnal.OneToOneUnidirectionalService;
import eu.davidfranck.jpa.onetoone.unidirectionnal.model.Address;
import eu.davidfranck.jpa.onetoone.unidirectionnal.model.BarUserWithAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.util.List;
import java.util.Map;

import static eu.davidfranck.jpa.onetoone.unidirectionnal.model.Address.AddressBuilder.createAddressBuilder;
import static eu.davidfranck.jpa.onetoone.unidirectionnal.model.BarUserWithAddress.BarUserWithUserBuilder.createBarUserBuilder;
import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;

/**
 * BarUserWithAddress has one Address.
 * No Cascade.
 * Unidirectional association.
 */
@SpringBootTest
public class OneToOneUnidirectionalWithoutCascadeOnParentTest {

    @Autowired
    private OneToOneUnidirectionalService service;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @AfterEach
    public void afterEach() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "bar_user_with_address", "address");
    }

    /**
     * We can't save BarUser with a transient address
     */
    @Test
    void create_user_with_transient_address_should_throw_exception() {
        // given
        Address address = createAddressBuilder().withValue("ADDRESS").build();
        BarUserWithAddress user = createBarUserBuilder().withName("BAR_USERNAME").withAddress(address).build();

        // when
        Exception exception = catchException(() -> service.saveBarUser(user));

        // then
        assertThat(exception)
                .hasMessage("org.hibernate.TransientPropertyValueException: object references an unsaved transient instance - save the transient instance before flushing : eu.davidfranck.jpa.onetoone.unidirectionnal.model.BarUserWithAddress.address -> eu.davidfranck.jpa.onetoone.unidirectionnal.model.Address")
                .isInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    @Test
    void create_user_with_attached_address() {
        // given
        Address address = createAddressBuilder().withValue("ADDRESS").build();
        service.saveAddress(address);

        BarUserWithAddress user = createBarUserBuilder().withName("BAR_USERNAME").withAddress(address).build();

        // when
        service.saveBarUser(user);

        // then
        List<Map<String, Object>> addressResult = jdbcTemplate.queryForList("select * from address");
        assertThat(addressResult)
                .singleElement()
                .satisfies(addressRow -> assertThat(addressRow).containsAnyOf(entry("ZE_VALUE", "ADDRESS")));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "ADDRESS")).isEqualTo(1);

        List<Map<String, Object>> userResult = jdbcTemplate.queryForList("select * from bar_user_with_address");
        assertThat(userResult)
                .singleElement()
                .satisfies(barUserRow -> assertThat(barUserRow).containsAnyOf(entry("NAME", "BAR_USERNAME")));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "BAR_USER_WITH_ADDRESS")).isEqualTo(1);
    }

    /**
     * Generated request at the "when" step :
     * <ul>
     * <li>select baruserwit0_.id as id1_1_0_, baruserwit0_.address_id as address_3_1_0_, baruserwit0_.name as name2_1_0_ from bar_user_with_address baruserwit0_ where baruserwit0_.id=?</li>
     * <li>select address0_.id as id1_0_0_, address0_.ze_value as ze_value2_0_0_ from address address0_ where address0_.id=?</li>
     * <li>update bar_user_with_address set address_id=?, name=? where id=?</li>
     * </ul>
     * => no update of the address
     */
    @Test
    void update_user_with_detached_address() {
        // given
        Address address = createAddressBuilder().withValue("ADDRESS").build();
        service.saveAddress(address);

        BarUserWithAddress user = createBarUserBuilder().withName("BAR_USERNAME").withAddress(address).build();
        service.saveBarUser(user);

        Address newAddress = createAddressBuilder().withId(address.getId()).withValue("NEW ADDRESS WILL NOT BE UPDATE").build();
        // when
        BarUserWithAddress updatedBarUser = createBarUserBuilder()
                .withId(user.getId())
                .withName("UPDATED_NAME")
                .withAddress(newAddress)
                .build();
        service.saveBarUser(updatedBarUser);

        // then
        List<Map<String, Object>> addressResult = jdbcTemplate.queryForList("select * from address");
        assertThat(addressResult)
                .singleElement()
                .satisfies(addressRow -> assertThat(addressRow).containsAnyOf(entry("ZE_VALUE", "ADDRESS")));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "ADDRESS")).isEqualTo(1);

        List<Map<String, Object>> userResult = jdbcTemplate.queryForList("select * from bar_user_with_address");
        assertThat(userResult)
                .singleElement()
                .satisfies(barUserRow -> assertThat(barUserRow).containsAnyOf(
                        entry("NAME", "UPDATED_NAME"),
                        entry("address_id", address.getId())));
        assertThat(JdbcTestUtils.countRowsInTable(jdbcTemplate, "BAR_USER_WITH_ADDRESS")).isEqualTo(1);
    }
}
