package eu.davidfranck.exception;

import io.vavr.CheckedFunction1;
import io.vavr.control.Either;
import io.vavr.control.Try;
import org.apache.commons.lang3.function.Failable;
import org.junit.jupiter.api.Test;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;

public class ExceptionInStreamTest {

    /**
     * Quand une checked exception est lancée au sein d'un stream ça alourdi la lisibilité,
     * car on doit la catcher.
     */
    @Test
    void streamWithException() {
        Exception exception = catchException(() -> Stream.of("java.lang.String", "ch.frankel.blog.Dummy", "java.util.ArrayList")
                .map(it -> {
                    try {
                        return Class.forName(it);
                    } catch (ClassNotFoundException e) {
                        throw new RuntimeException("ko");
                    }
                })
                .forEach(System.out::println));
        assertThat(exception).hasMessage("ko");
    }

    /**
     * Une solution au problème précédent.
     * Avec commons.lang3.
     * Par contre, ça remonte une exception wrappée.
     * Et on ne peut pas continuer le stream si on veut.
     */
    @Test
    void failableStream() {
        Stream<String> stream = Stream.of("java.lang.String", "ch.frankel.blog.Dummy", "java.util.ArrayList");
        Exception exception = catchException(() -> Failable.stream(stream)
                .map(Class::forName)
                .forEach(System.out::println));
        assertThat(exception).isInstanceOf(UndeclaredThrowableException.class);
    }

    @Test
    void vavrSolutionToExceptionInStream() {
        var result = io.vavr.collection.Stream.of("java.lang.String", "ch.frankel.blog.Dummy", "java.util.ArrayList")
                .map(CheckedFunction1.liftTry(Class::forName))
                .map(Try::toEither)
                .partition(Either::isLeft)
                .map1(left -> left.map(Either::getLeft))
                .map2(right -> right.map(Either::get));
        result._1().forEach(it -> System.out.println("not found: " + it.getMessage()));
        result._2().forEach(it -> System.out.println("class: " + it.getName()));
    }
}
