package eu.davidfranck.realnumbers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

/**
 * Le nombre de réels est infini, alors qu'il y a un nombre fini de valeurs
 * réprésentables par un ordinateur. La plupart des valeurs réelles ne sont donc
 * qu'approximatives avec des virgules flottantes.
 */
public class FloatTest {

    /**
     * https://en.wikipedia.org/wiki/Floating-point_arithmetic :
     * <p>En informatique, l'arithmétique à virgule flottante est une arithmétique qui représente les
     * nombres réels de manière approximative, en utilisant un nombre entier avec une précision fixe,
     * appelé mantisse (significand en anglais), mis à l'échelle par un exposant entier d'une base fixe.</p>
     * <p>Ex : 12.345 = 12345 x 10^-3 (12345 = mantisse, 10 = base, -3 = exposant)</p>
     * <p> C'est bien expliqué ici aussi : https://docs.python.org/fr/3/tutorial/floatingpoint.html</p>
     */
    @Test
    void whatIsAFloat() {
        assertThat(12.345).isEqualTo(12.345);
    }

    /**
     * https://dzone.com/articles/never-use-float-and-double-for-monetary-calculatio.
     * <p>"Le fait qu'un nombre rationnel ait ou non une expansion terminale dépend de la base. Par
     * exemple, en base 10, le nombre 1/2 a une expansion terminale (0,5) alors que le nombre 1/3 n'en
     * a pas (0,333...). <b>En base 2, seuls les rationnels dont les dénominateurs sont des puissances
     * de 2 (comme 1/2 ou 3/16) sont terminatifs. Tout rationnel dont le dénominateur a un facteur
     * premier autre que 2 aura un développement binaire infini</b>. Cela signifie que les nombres qui
     * semblent courts et exacts lorsqu'ils sont écrits au format décimal peuvent devoir être
     * approximés lorsqu'ils sont convertis en nombres binaires à virgule flottante. Par exemple, le
     * nombre décimal 0,1 n'est pas représentable en virgule flottante binaire avec une précision
     * finie ; la représentation binaire exacte aurait une séquence "1100" qui se poursuivrait sans
     * fin.</p>
     * <p>
     * e = −4; s = 1100110011001100110011001100110011…, s est la significande et
     * e l'exposant
     * </p>
     * <p>Quand on arrondi à 24 bits cela devient e = −4; s = 110011001100110011001101, ce qui est
     * 0.100000001490116119384765625 en decimal."</p>
     * </p>
     */
    @Test
    void floatPrecisionIsNotAlwaysPossible() {
        // si on ne mt pas "f" on c'est pas défaut du double
        assertThat(0.1f + 0.2f).isEqualTo(0.3f);
        assertThat(0.10d + 0.20d).isEqualTo(0.30000000000000004);
    }

    @Nested
    class Fractions {

        private static final float HALF = 1 / 2f;
        private static final float THIRD = 1 / 3f;
        private static final float QUARTER = 1 / 4f;
        private static final float FIFTH = 1 / 5f;
        private static final float SIXTH = 1 / 6f;
        private static final float SEVENTH = 1 / 7f;

        private static final int FACTOR = 840;

        @Test
        void precisionLost() {
            assertThat(HALF).isEqualTo(0.5f);
            assertThat(THIRD).isEqualTo(0.33333334f);
            assertThat(THIRD).isEqualTo(0.33333335f); // WTF ?
            assertThat(THIRD).isEqualTo(0.33333333f); // WTF ?
            assertThat(QUARTER).isEqualTo(0.25f);
            assertThat(FIFTH).isEqualTo(0.2f);
            assertThat(SIXTH).isEqualTo(0.16666667f);
            assertThat(SIXTH).isNotEqualTo(0.16666666f);
            assertThat(SEVENTH).isEqualTo(0.14285715f);

            float sum = 0;

            for (int i = 0; i < FACTOR; ++i) sum += HALF;
            assertThat(sum).as("no precision lost").isEqualTo(FACTOR / 2);

            sum = 0;
            for (int i = 0; i < FACTOR; ++i) sum += THIRD;
            assertThat(sum).as("should be 280 but is 279.99915").isNotEqualTo(FACTOR / 3);

            sum = 0;
            for (int i = 0; i < FACTOR; ++i) sum += QUARTER;
            assertThat(sum).as("no precision lost").isEqualTo(FACTOR / 4);

            sum = 0;
            for (int i = 0; i < FACTOR; ++i) sum += FIFTH;
            assertThat(sum).as("should be 168 but is 167.99858").isNotEqualTo(FACTOR / 5);

            // etc
        }
    }
}
