package eu.davidfranck.realnumbers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Pour contourner les limitations de float. Voir {@link FloatTest}.
 */
public class BigDecimalTest {

    @Test
    void doNotUseBigDecimalWithDoubleConstructor() {
        assertThat(new BigDecimal(0.1d).toString()).isEqualTo(
                "0.1000000000000000055511151231257827021181583404541015625");
        assertThat(new BigDecimal("0.1").toString()).isEqualTo("0.1");
    }

    /**
     * <p>La précision est le nombre total de chiffres (ou chiffres significatifs) d'un nombre réel.
     * L'échelle spécifie le nombre de chiffres après la décimale.</p>
     * <p>Par exemple, 12.345 a une précision de 5 (nombre total de chiffres) et une échelle de 3
     * (nombre de chiffres à droite de la décimale).</p>
     */
    @Test
    void precisionAndScale() {
        BigDecimal bigDecimal = new BigDecimal("12.345");
        assertThat(bigDecimal.precision()).isEqualTo(5);
        assertThat(bigDecimal.scale()).isEqualTo(3);
    }

    @Nested
    class PrecisionCanBeModifiedDuringAnOperation {

        @Test
        void precisionIsAddedWhenMultiply() {
            BigDecimal net = new BigDecimal("123.45");
            BigDecimal tax = new BigDecimal("1.234");
            BigDecimal multiply = net.multiply(tax);
            assertThat(multiply.scale()).isEqualTo(net.scale() + tax.scale());
            assertThat(multiply.toString()).isEqualTo("152.33730");
        }

        @Test
        void precisionIsMaxWhenAddOrSubstract() {
            BigDecimal net = new BigDecimal("123.45");
            BigDecimal tax = new BigDecimal("1.234");
            int maxScale = Math.max(net.scale(), tax.scale());
            assertThat(net.add(tax).scale()).isEqualTo(maxScale);
            assertThat(net.subtract(tax).scale()).isEqualTo(maxScale);
        }

        @Test
        void precisionIsModifiedWhenDivide() {
            BigDecimal dividend = new BigDecimal("4.5");
            BigDecimal divisor = new BigDecimal("0.25");
            assertThat(dividend.scale()).isEqualTo(1);
            assertThat(divisor.scale()).isEqualTo(2);
            assertThat(dividend.divide(divisor).toString()).isEqualTo("18");
            assertThat(dividend.divide(divisor).scale()).isEqualTo(0); // dans la doc je vois dividend.scale() - divisor.scale() ce qui devrait donner -1
        }
    }

    /**
     * Par défaut les BigDecimal on un {@link java.math.MathContext} à UNLIMITED. On va alors toujours
     * essayer de faire le calcul qui retourne le résultat précis. Avec une division de 1/3 on ne peut
     * pas faire le calcul car il n'y a pas d'expension terminale.
     */
    @Test
    void exceptionIsThrownWhenDivideWithoutMathContextAndNonTerminatingTerminalExpansion() {
        BigDecimal net = new BigDecimal("1");
        BigDecimal tax = new BigDecimal("3");
        Exception exception = catchException(() -> net.divide(tax));
        assertThat(exception).isInstanceOf(ArithmeticException.class);
    }

    @Nested
    class Divide {
        @Test
        void divideWithRound() {
            BigDecimal net = new BigDecimal("1");
            BigDecimal tax = new BigDecimal("3");
            BigDecimal divide = net.divide(tax, 2, RoundingMode.HALF_UP);
            assertThat(divide.toString()).isEqualTo("0.33");
        }

        @Test
        void divideWithMathContext() {
            BigDecimal net = new BigDecimal("1");
            BigDecimal tax = new BigDecimal("3");
            BigDecimal divideMathContextDecimal32 = net.divide(tax, MathContext.DECIMAL32);
            assertThat(divideMathContextDecimal32.toString()).isEqualTo("0.3333333");

            BigDecimal divideMathContextDecimal64 = net.divide(tax, MathContext.DECIMAL64);
            assertThat(divideMathContextDecimal64.toString()).isEqualTo("0.3333333333333333");

            BigDecimal divideMathContextDecimal128 = net.divide(tax, MathContext.DECIMAL128);
            assertThat(divideMathContextDecimal128.toString()).isEqualTo(
                    "0.3333333333333333333333333333333333");
        }
    }

    @Test
    void equalsAndCompareDifference() {
        BigDecimal x = new BigDecimal("1");// scale 0 precision 1
        BigDecimal y = new BigDecimal("1.0");// scale 1 precision 2
        assertThat(x).as("equals se base sur la valeur et aussi l'échelle").isNotEqualTo(y);
        assertThat(y).isEqualTo(new BigDecimal("1.0"));
        assertThat(x.compareTo(y)).as("compareTo se base uniquement sur la valeur").isEqualTo(0);
        assertThat(y.precision()).isEqualTo(2);
        assertThat(x.precision()).isEqualTo(1);
    }

    @Test
    void mathContext() {
        assertThat(new BigDecimal("34.56")
                .round(new MathContext(2, RoundingMode.HALF_UP)).toString())
                .as("MathContext réduit la précision (et non l'échelle !) à 2, en prenant en compte l'arrondi indiqué")
                .isEqualTo("35");
        assertThat(new BigDecimal("3.44")
                .round(new MathContext(2, RoundingMode.HALF_UP)).toString())
                .isEqualTo("3.4");
    }

    @Test
    void setScaleWithoutRoundingMode() {
        assertThat(new BigDecimal("1.23").setScale(3).toString())
                .as("pas de RoundingMode valorisé, donc cela utilise ROUND_UNNECESSARY qui est déprécié")
                .isEqualTo("1.230");

        Exception exception = catchException(() -> assertThat(new BigDecimal("1.23").setScale(1)));
        assertThat(exception)
                .as("on aurait une perte de précision avec ROUND_UNNECESSARY donc cela lance une exception")
                .isInstanceOf(ArithmeticException.class);
    }

    @Test
    void setScaleWithRoundingMode() {
        assertThat(new BigDecimal("1.23").setScale(1, RoundingMode.HALF_UP).toString())
                .isEqualTo("1.2");
        assertThat(new BigDecimal("1.23").setScale(3, RoundingMode.HALF_UP).toString())
                .isEqualTo("1.230");
        assertThat(new BigDecimal("1.23").setScale(1, RoundingMode.CEILING).toString())
                .as("arrondi au supérieur")
                .isEqualTo("1.3");
    }
}
