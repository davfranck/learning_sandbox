package eu.davidfranck.encoding;

import org.assertj.core.data.MapEntry;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.SortedMap;

import static org.assertj.core.api.Assertions.assertThat;

public class EncodingTest {

    @Test
    void defaultCharSet() {
        String displayName = Charset.defaultCharset().displayName();
        String osname = System.getProperty("os.name");
        // not tested for windows
        String expectedEncoding = osname.equalsIgnoreCase("linux") ? "UTF-8" : "cp1252";
        assertThat(displayName).isEqualTo(expectedEncoding);
    }

    @Test
    void availableCharsets() {
        SortedMap<String, Charset> availableCharsets = Charset.availableCharsets();
        assertThat(availableCharsets).isNotEmpty().contains(MapEntry.entry("UTF-8", Charset.defaultCharset()));
    }

    @Test
    void filesReadAllLines() throws IOException {
        List<String> allLines = Files.readAllLines(Path.of("src", "test", "resources", "test.txt"));
        assertThat(allLines).containsExactly("Décès", "Coucou");
    }

    @Test
    void death() {
        String format = String.format("%040x", new BigInteger(1, "Décès".getBytes(Charset.forName("UTF-8"))));
        assertThat(format).isEqualTo("0000000000000000000000000044c3a963c3a873");
    }

}
