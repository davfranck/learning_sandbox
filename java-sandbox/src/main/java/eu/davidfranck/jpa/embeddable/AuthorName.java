package eu.davidfranck.jpa.embeddable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

@Embeddable
public class AuthorName {

    private String firstName;
    @Column(name = "familyName")
    private String lastName;

    public AuthorName() {
    }

    public AuthorName(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
