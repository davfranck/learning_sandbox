package eu.davidfranck.jpa.embeddable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Author {

    @Id
    @GeneratedValue
    private Integer id;

    private String something;

    private AuthorName name;

    public Author(String something, AuthorName authorName) {
        this.something = something;
        this.name = authorName;
    }
}
