package eu.davidfranck.jpa.version.bidirectional.repository;

import eu.davidfranck.jpa.version.bidirectional.model.ParentVersion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidirectionalParentRepository extends JpaRepository<ParentVersion, Long> {
}
