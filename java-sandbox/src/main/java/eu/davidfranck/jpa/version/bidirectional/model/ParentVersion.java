package eu.davidfranck.jpa.version.bidirectional.model;

import jakarta.persistence.*;
import org.hibernate.annotations.OptimisticLock;

import java.util.ArrayList;
import java.util.List;

@Entity(name = "ParentVersionBidirectional")
public class ParentVersion {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String parentValue;

    @Version
    private int version;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ChildVersion> childVersions = new ArrayList<>();

    public ParentVersion() {
    }

    public ParentVersion(String parentValue) {
        this.parentValue = parentValue;
    }

    public int getVersion() {
        return version;
    }

    public void setParentValue(String parentValue) {
        this.parentValue = parentValue;
    }

    public Long getId() {
        return id;
    }

    public void addChidlVersion(ChildVersion childVersion) {
        childVersions.add(childVersion);
        childVersion.setParentVersion(this);
    }
}
