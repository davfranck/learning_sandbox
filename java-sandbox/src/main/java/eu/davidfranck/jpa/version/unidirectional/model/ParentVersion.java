package eu.davidfranck.jpa.version.unidirectional.model;

import jakarta.persistence.*;

@Entity
public class ParentVersion {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String parentValue;

    @Version
    private int version;

    public ParentVersion() {
    }

    public ParentVersion(String parentValue) {
        this.parentValue = parentValue;
    }

    public int getVersion() {
        return version;
    }

    public void setParentValue(String parentValue) {
        this.parentValue = parentValue;
    }

    public Long getId() {
        return id;
    }
}
