package eu.davidfranck.jpa.version.bidirectional.repository;

import eu.davidfranck.jpa.version.bidirectional.model.ChildVersion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidirectionalChildRepository extends JpaRepository<ChildVersion, Long> {
}
