package eu.davidfranck.jpa.version.unidirectional.repository;

import eu.davidfranck.jpa.version.unidirectional.model.ParentVersion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnidirectionalParentRepository extends JpaRepository<ParentVersion, Long> {
}
