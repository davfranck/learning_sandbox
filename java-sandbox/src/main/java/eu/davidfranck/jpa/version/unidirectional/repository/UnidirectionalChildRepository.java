package eu.davidfranck.jpa.version.unidirectional.repository;

import eu.davidfranck.jpa.version.unidirectional.model.ChildVersion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnidirectionalChildRepository extends JpaRepository<ChildVersion, Long> {
}
