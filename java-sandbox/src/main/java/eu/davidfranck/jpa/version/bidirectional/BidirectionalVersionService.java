package eu.davidfranck.jpa.version.bidirectional;

import eu.davidfranck.jpa.version.bidirectional.model.ChildVersion;
import eu.davidfranck.jpa.version.bidirectional.model.ParentVersion;
import eu.davidfranck.jpa.version.bidirectional.repository.BidirectionalChildRepository;
import eu.davidfranck.jpa.version.bidirectional.repository.BidirectionalParentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BidirectionalVersionService {

    private final BidirectionalParentRepository bidirectionalParentRepository;
    private final BidirectionalChildRepository bidirectionalChildRepository;

    public BidirectionalVersionService(BidirectionalParentRepository bidirectionalParentRepository, BidirectionalChildRepository bidirectionalChildRepository) {
        this.bidirectionalParentRepository = bidirectionalParentRepository;
        this.bidirectionalChildRepository = bidirectionalChildRepository;
    }

    @Transactional
    public void saveParent(ParentVersion parentVersion) {
        bidirectionalParentRepository.save(parentVersion);
    }

    @Transactional
    public void saveChild(ChildVersion childVersion) {
        bidirectionalChildRepository.save(childVersion);
    }

    @Transactional
    public ParentVersion getParent(Long id) {
       return bidirectionalParentRepository.findById(id).get();
    }

    @Transactional
    public ChildVersion getChild(Long id) {
        return bidirectionalChildRepository.findById(id).get();
    }
}
