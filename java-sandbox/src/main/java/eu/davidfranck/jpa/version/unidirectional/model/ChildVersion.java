package eu.davidfranck.jpa.version.unidirectional.model;

import jakarta.persistence.*;

@Entity
public class ChildVersion {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String childValue;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private ParentVersion parent;

    @Version
    @Column
    private Integer version;

    public ChildVersion() {
    }

    public ChildVersion(String childValue, ParentVersion parent) {
        this.childValue = childValue;
        this.parent = parent;
    }

    public Integer getVersion() {
        return version;
    }

    public Long getId() {
        return id;
    }
}
