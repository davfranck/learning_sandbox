package eu.davidfranck.jpa.version.bidirectional.model;

import jakarta.persistence.*;

@Entity(name = "ChildVersionBidirectional")
public class ChildVersion {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String childValue;

    @ManyToOne
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    private ParentVersion parent;

    @Version
    @Column
    private Integer version;

    public ChildVersion() {
    }

    public ChildVersion(String childValue, ParentVersion parent) {
        this.childValue = childValue;
        this.parent = parent;
    }

    public Integer getVersion() {
        return version;
    }

    public Long getId() {
        return id;
    }

    public void setParentVersion(ParentVersion parentVersion) {
        this.parent = parentVersion;
    }
}
