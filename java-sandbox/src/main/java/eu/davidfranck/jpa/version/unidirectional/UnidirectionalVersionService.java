package eu.davidfranck.jpa.version.unidirectional;

import eu.davidfranck.jpa.version.unidirectional.model.ChildVersion;
import eu.davidfranck.jpa.version.unidirectional.model.ParentVersion;
import eu.davidfranck.jpa.version.unidirectional.repository.UnidirectionalChildRepository;
import eu.davidfranck.jpa.version.unidirectional.repository.UnidirectionalParentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UnidirectionalVersionService {

    private final UnidirectionalParentRepository unidirectionalParentRepository;
    private final UnidirectionalChildRepository unidirectionalChildRepository;

    public UnidirectionalVersionService(UnidirectionalParentRepository unidirectionalParentRepository, UnidirectionalChildRepository unidirectionalChildRepository) {
        this.unidirectionalParentRepository = unidirectionalParentRepository;
        this.unidirectionalChildRepository = unidirectionalChildRepository;
    }

    @Transactional
    public void saveParent(ParentVersion parentVersion) {
        unidirectionalParentRepository.save(parentVersion);
    }

    @Transactional
    public void saveChild(ChildVersion childVersion) {
        unidirectionalChildRepository.save(childVersion);
    }

    @Transactional
    public ParentVersion getParent(Long id) {
       return unidirectionalParentRepository.findById(id).get();
    }

    @Transactional
    public ChildVersion getChild(Long id) {
        return unidirectionalChildRepository.findById(id).get();
    }
}
