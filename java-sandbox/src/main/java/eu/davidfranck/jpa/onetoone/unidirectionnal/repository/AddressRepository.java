package eu.davidfranck.jpa.onetoone.unidirectionnal.repository;

import eu.davidfranck.jpa.onetoone.unidirectionnal.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
