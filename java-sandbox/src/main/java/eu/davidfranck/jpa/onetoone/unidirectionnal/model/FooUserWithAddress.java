package eu.davidfranck.jpa.onetoone.unidirectionnal.model;

import jakarta.persistence.*;

// User is not a valid table name
@Entity
public class FooUserWithAddress {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    public FooUserWithAddress() {
    }

    public FooUserWithAddress(FooUserWithAddressBuilder builder) {
        this.address = builder.address;
        this.name = builder.name;
        this.id = builder.id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public static class FooUserWithAddressBuilder {

        private String name;
        private Address address;
        private Long id;

        public static FooUserWithAddressBuilder createFooUserWithAddressBuilder(){
            return new FooUserWithAddressBuilder();
        }

        public FooUserWithAddressBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public FooUserWithAddressBuilder withAddress(Address address) {
            this.address = address;
            return this;
        }

        public FooUserWithAddressBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public FooUserWithAddress build() {
            return new FooUserWithAddress(this);
        }
    }
}
