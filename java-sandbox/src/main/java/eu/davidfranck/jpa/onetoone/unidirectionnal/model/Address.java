package eu.davidfranck.jpa.onetoone.unidirectionnal.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Address {

    @Id
    @GeneratedValue
    private Long id;

    // value is not a valid column name
    @Column(name = "zeValue")
    private String value;

    public Address() {
    }


    public Address(AddressBuilder builder) {
        this.id = builder.id;
        this.value = builder.value;
    }

    public Long getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public static class AddressBuilder {

        private String value;
        private Long id;

        private AddressBuilder() {
        }

        public static AddressBuilder createAddressBuilder(){
            return new AddressBuilder();
        }
        public AddressBuilder withValue(String value) {
            this.value = value;
            return this;
        }

        public AddressBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public Address build() {
            return new Address(this);
        }
    }
}
