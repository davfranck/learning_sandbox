package eu.davidfranck.jpa.onetoone.bidirectionnal.model;

import jakarta.persistence.*;

// User is not a valid table name
@Entity
public class UserWithPhone {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToOne(
            mappedBy = "userWithPhone",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    private Phone phone;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Phone getPhone() {
        return phone;
    }

    public UserWithPhone() {
    }

    public UserWithPhone(UserWithPhoneBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.phone = builder.phone;
    }

    public void addPhone(Phone phoneToUpdate) {
        this.phone = phoneToUpdate;
        phoneToUpdate.setUserWithPhone(this);
    }

    public static class UserWithPhoneBuilder {

        private String name;
        private Phone phone;
        private Long id;

        public static UserWithPhoneBuilder createUserWithPhoneBuilder() {
            return new UserWithPhoneBuilder();
        }

        public UserWithPhoneBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public UserWithPhoneBuilder withPhone(Phone phone) {
            this.phone = phone;
            return this;
        }

        public UserWithPhoneBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public UserWithPhone build() {
            return new UserWithPhone(this);
        }
    }
}
