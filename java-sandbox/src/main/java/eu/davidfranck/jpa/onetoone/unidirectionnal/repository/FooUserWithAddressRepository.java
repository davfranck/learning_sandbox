package eu.davidfranck.jpa.onetoone.unidirectionnal.repository;

import eu.davidfranck.jpa.onetoone.unidirectionnal.model.FooUserWithAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FooUserWithAddressRepository extends JpaRepository<FooUserWithAddress, Long> {
}
