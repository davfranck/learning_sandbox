package eu.davidfranck.jpa.onetoone.unidirectionnal.repository;

import eu.davidfranck.jpa.onetoone.unidirectionnal.model.BarUserWithAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BarUserWithAddressRepository extends JpaRepository<BarUserWithAddress, Long> {
}
