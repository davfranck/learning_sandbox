package eu.davidfranck.jpa.onetoone.bidirectionnal.model;

import jakarta.persistence.*;

@Entity
public class Phone {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "zeValue")
    private String value;

    @OneToOne
    @JoinColumn(name = "user_with_phone_id", nullable = false)
    private UserWithPhone userWithPhone;

    public Phone() {
    }

    public Phone(PhoneBuilder builder) {
        this.id = builder.id;
        this.value = builder.value;
        this.userWithPhone = builder.userWithPhone;
    }

    public Long getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public UserWithPhone getUserWithPhone() {
        return userWithPhone;
    }

    public void setUserWithPhone(UserWithPhone userWithPhone) {
        this.userWithPhone = userWithPhone;
    }

    public static class PhoneBuilder {

        private String value;
        private Long id;
        private UserWithPhone userWithPhone;

        public static PhoneBuilder createPhoneBuilder() {
            return new PhoneBuilder();
        }

        public PhoneBuilder withValue(String value) {
            this.value = value;
            return this;
        }

        public PhoneBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public PhoneBuilder withUserWithPhone(UserWithPhone userWithPhone) {
            this.userWithPhone = userWithPhone;
            return this;
        }

        public Phone build() {
            return new Phone(this);
        }
    }
}
