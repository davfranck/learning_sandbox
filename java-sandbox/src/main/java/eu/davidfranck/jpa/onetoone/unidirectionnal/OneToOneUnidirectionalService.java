package eu.davidfranck.jpa.onetoone.unidirectionnal;

import eu.davidfranck.jpa.onetoone.unidirectionnal.model.Address;
import eu.davidfranck.jpa.onetoone.unidirectionnal.model.BarUserWithAddress;
import eu.davidfranck.jpa.onetoone.unidirectionnal.model.FooUserWithAddress;
import eu.davidfranck.jpa.onetoone.unidirectionnal.repository.AddressRepository;
import eu.davidfranck.jpa.onetoone.unidirectionnal.repository.BarUserWithAddressRepository;
import eu.davidfranck.jpa.onetoone.unidirectionnal.repository.FooUserWithAddressRepository;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;

@Service
public class OneToOneUnidirectionalService {

    private final FooUserWithAddressRepository fooUserWithAddressRepository;
    private final BarUserWithAddressRepository barUserWithAddressRepository;

    private final AddressRepository addressRepository;

    public OneToOneUnidirectionalService(FooUserWithAddressRepository fooUserWithAddressRepository, BarUserWithAddressRepository barUserWithAddressRepository, AddressRepository addressRepository) {
        this.fooUserWithAddressRepository = fooUserWithAddressRepository;
        this.barUserWithAddressRepository = barUserWithAddressRepository;
        this.addressRepository = addressRepository;
    }


    @Transactional
    public void saveFoo(FooUserWithAddress user) {
        fooUserWithAddressRepository.save(user);
    }

    @Transactional
    public void saveBarUser(BarUserWithAddress user) {
        barUserWithAddressRepository.save(user);
    }

    @Transactional
    public void saveAddress(Address address) {
        addressRepository.save(address);
    }

}
