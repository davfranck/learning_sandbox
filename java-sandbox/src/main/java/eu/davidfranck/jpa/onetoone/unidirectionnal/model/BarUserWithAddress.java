package eu.davidfranck.jpa.onetoone.unidirectionnal.model;

import jakarta.persistence.*;

@Entity
public class BarUserWithAddress {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToOne// No cascade
    @JoinColumn(name = "address_id")
    private Address address;

    public BarUserWithAddress() {
    }

    public BarUserWithAddress(BarUserWithUserBuilder builder) {
        this.address = builder.address;
        this.name = builder.name;
        this.id = builder.id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public static class BarUserWithUserBuilder {

        private String name;
        private Address address;
        private Long id;

        private BarUserWithUserBuilder() {
        }

        public static BarUserWithUserBuilder createBarUserBuilder(){
            return new BarUserWithUserBuilder();
        }

        public BarUserWithUserBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public BarUserWithUserBuilder withAddress(Address address) {
            this.address = address;
            return this;
        }

        public BarUserWithUserBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public BarUserWithAddress build() {
            return new BarUserWithAddress(this);
        }
    }
}
