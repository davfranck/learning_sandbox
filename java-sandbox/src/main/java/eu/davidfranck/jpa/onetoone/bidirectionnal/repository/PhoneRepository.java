package eu.davidfranck.jpa.onetoone.bidirectionnal.repository;

import eu.davidfranck.jpa.onetoone.bidirectionnal.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<Phone, Long> {
}
