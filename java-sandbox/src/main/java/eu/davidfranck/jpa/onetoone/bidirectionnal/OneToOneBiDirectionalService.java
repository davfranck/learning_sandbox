package eu.davidfranck.jpa.onetoone.bidirectionnal;

import eu.davidfranck.jpa.onetoone.bidirectionnal.model.Phone;
import eu.davidfranck.jpa.onetoone.bidirectionnal.model.UserWithPhone;
import eu.davidfranck.jpa.onetoone.bidirectionnal.repository.PhoneRepository;
import eu.davidfranck.jpa.onetoone.bidirectionnal.repository.UserWithPhoneRepository;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;

@Service
public class OneToOneBiDirectionalService {

    private final UserWithPhoneRepository userWithPhoneRepository;

    private final PhoneRepository phoneRepository;

    public OneToOneBiDirectionalService(UserWithPhoneRepository userWithPhoneRepository, PhoneRepository phoneRepository) {
        this.userWithPhoneRepository = userWithPhoneRepository;
        this.phoneRepository = phoneRepository;
    }


    @Transactional
    public void saveUserWithPhone(UserWithPhone user) {
        userWithPhoneRepository.save(user);
    }

    public void savePhone(Phone phone) {
        phoneRepository.save(phone);
    }

    public void savePhone(Phone phone, Long userWithPhoneId) {
        phone.setUserWithPhone(userWithPhoneRepository.getReferenceById(userWithPhoneId));
        phoneRepository.save(phone);
    }
}
