package eu.davidfranck.jpa.onetoone.bidirectionnal.repository;

import eu.davidfranck.jpa.onetoone.bidirectionnal.model.UserWithPhone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserWithPhoneRepository extends JpaRepository<UserWithPhone, Long> {
}
