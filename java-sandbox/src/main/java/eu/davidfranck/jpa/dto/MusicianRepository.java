package eu.davidfranck.jpa.dto;

import eu.davidfranck.jpa.dto.dto.MusicianDto;
import eu.davidfranck.jpa.dto.model.Musician;
import java.util.List;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MusicianRepository extends JpaRepository<Musician, Long> {

  @EntityGraph(attributePaths = "albums")
  List<Musician> findMusiciansWithAlbumsBy();

  @Query("SELECT m FROM Musician m JOIN FETCH m.albums a" )
  List<MusicianDto> findAllMusiciansDto();
}
