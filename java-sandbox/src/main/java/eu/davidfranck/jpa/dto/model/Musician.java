package eu.davidfranck.jpa.dto.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import java.util.List;

@Entity
public class Musician {
  @Id
  @GeneratedValue
  private Long id;

  @Column
  private String name;
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "address_id")
  private MusicianAddress address;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Album> albums;

  public Musician() {
  }

  public Musician(Long id, String name, MusicianAddress address, List<Album> albums) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.albums = albums;
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public MusicianAddress getAddress() {
    return address;
  }

  public List<Album> getAlbums() {
    return albums;
  }
}
