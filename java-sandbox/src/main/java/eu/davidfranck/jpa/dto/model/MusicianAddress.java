package eu.davidfranck.jpa.dto.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class MusicianAddress {
  @Id
  @GeneratedValue
  private Long id;
  @Column(name = "zeValue")
  private String value;

  public MusicianAddress() {
  }

  public MusicianAddress(String value) {
    this.value = value;
  }

  public Long getId() {
    return id;
  }

  public String getValue() {
    return value;
  }
}
