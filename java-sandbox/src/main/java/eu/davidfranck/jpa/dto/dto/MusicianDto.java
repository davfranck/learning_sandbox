package eu.davidfranck.jpa.dto.dto;

import java.util.List;

public interface MusicianDto {
  Long getId();
  String getName();
  List<AlbumDto> getAlbums();

  interface AlbumDto {
    String getTitle();
  }

  interface MusicianAddressDto {
    String getValue();
  }
}
