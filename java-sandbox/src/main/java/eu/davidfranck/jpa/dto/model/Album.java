package eu.davidfranck.jpa.dto.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Album {

  @Id
  @GeneratedValue
  private Long id;
  @Column
  private String title;

  public Album() {
  }

  public Album(String title) {
    this.title = title;
  }

  public Long getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }
}
