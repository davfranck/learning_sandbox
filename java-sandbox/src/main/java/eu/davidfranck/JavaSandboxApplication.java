package eu.davidfranck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class JavaSandboxApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSandboxApplication.class, args);
	}

}
