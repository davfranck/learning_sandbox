package eu.davidfranck.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "SampleFooClient", url = "http://localhost:9099")
public interface SampleFeignClient {

  @GetMapping("/foo")
  FooFeign getFoo();
}
