package eu.davidfranck.cqrs.alemaire.core.middleware.query;

import eu.davidfranck.cqrs.alemaire.core.Query;

public interface QueryBusMiddleware<R, T extends  Query<R>> {

    R dispatch(T query);
}
