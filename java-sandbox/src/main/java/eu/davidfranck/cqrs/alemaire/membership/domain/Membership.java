package eu.davidfranck.cqrs.alemaire.membership.domain;

public class Membership {

    private Long id;
    private String firstname;
    private String lastname;

    public Membership(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}
