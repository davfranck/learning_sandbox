package eu.davidfranck.cqrs.alemaire.membership.event;

import eu.davidfranck.cqrs.alemaire.core.EventHandler;
import eu.davidfranck.cqrs.alemaire.subscription.domain.MemberJoined;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SendWelcomeMailMemberJoinedEventHandler implements EventHandler<MemberJoined> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendWelcomeMailMemberJoinedEventHandler.class);

    @Override
    public Class<MemberJoined> listenTo() {
        return MemberJoined.class;
    }

    @Override
    public void handle(MemberJoined event) {
        // on peut injecter les repositories dont on a besoin et chercher des infos pour l'envoi du mail
        LOGGER.info("email sent to member " + event.newMemberId());
    }
}
