package eu.davidfranck.cqrs.alemaire.subscription.domain;

import java.time.LocalDate;
import java.util.List;

public class SubscriptionPlan {
    // Préférer normalement un UUID pour être indépendant de la base de données pour la génération
    private Long id;
    private Long price;
    private String name;
    private LocalDate expirationDate;
    private Long annualDiscount;
    List<Long> includedActivities;

    public SubscriptionPlan() {
    }

    public SubscriptionPlan(SubscriptionPlanBuilder builder) {
        this.price = builder.getPrice();
        this.name = builder.getName();
        this.expirationDate = builder.getExpirationDate();
        this.annualDiscount = builder.getAnnualDiscount();
        this.includedActivities = builder.getIncludedActivities();
    }


    public Long getId() {
        return this.id;
    }

    public Long getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public Long getAnnualDiscount() {
        return annualDiscount;
    }

    public List<Long> getIncludedActivities() {
        return includedActivities;
    }
}
