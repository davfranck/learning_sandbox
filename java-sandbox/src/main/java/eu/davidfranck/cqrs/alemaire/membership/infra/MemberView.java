package eu.davidfranck.cqrs.alemaire.membership.infra;

public record MemberView(Long id, String firstname, String lastname) {
}
