package eu.davidfranck.cqrs.alemaire.core;

import java.util.List;

public record CommandResponse(Long id, List<Event> events) {
    public CommandResponse(Long id) {
        this(id, List.of());
    }

    public boolean hasEvent() {
        return !events.isEmpty();
    }
}
