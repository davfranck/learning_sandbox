package eu.davidfranck.cqrs.alemaire.core.middleware.command;

import eu.davidfranck.cqrs.alemaire.core.Command;
import eu.davidfranck.cqrs.alemaire.core.CommandResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerMiddleware implements CommandBusMiddleware {

    private final CommandBusMiddleware next;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerMiddleware.class);

    public LoggerMiddleware(CommandBusMiddleware next) {
        this.next = next;
    }

    @Override
    public CommandResponse dispatch(Command command) {
        long startTime = System.nanoTime();
        CommandResponse commandResponse = this.next.dispatch(command);
        long endTime = System.nanoTime();
        LOGGER.info("Execution time in nano : {}", endTime - startTime);
        return commandResponse;
    }
}
