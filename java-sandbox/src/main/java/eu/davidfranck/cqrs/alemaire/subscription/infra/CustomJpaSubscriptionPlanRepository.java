package eu.davidfranck.cqrs.alemaire.subscription.infra;

import eu.davidfranck.cqrs.alemaire.subscription.domain.ISubscriptionPlanRepository;
import eu.davidfranck.cqrs.alemaire.subscription.domain.SubscriptionPlan;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class CustomJpaSubscriptionPlanRepository implements ISubscriptionPlanRepository {

    private final SubscriptionJpaRepository repository;

    public CustomJpaSubscriptionPlanRepository(SubscriptionJpaRepository repository) {
        this.repository = repository;
    }

    @Override
    public Long save(SubscriptionPlan subscriptionPlan) {
        JpaSubscriptionPlan saved = repository.save(new JpaSubscriptionPlan(subscriptionPlan));
        return saved.getId();
    }

    @Override
    public List<SubscriptionPlan> findActivePlan() {
        return repository.findAllByExpirationDate(LocalDate.now())
                .stream()
                .map(JpaSubscriptionPlan::toEntity)
                .toList();
    }
}
