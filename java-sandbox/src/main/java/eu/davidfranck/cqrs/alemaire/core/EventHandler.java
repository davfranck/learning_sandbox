package eu.davidfranck.cqrs.alemaire.core;

public interface EventHandler<T extends Event> {
    Class<T> listenTo();

    void handle(T event);
}
