package eu.davidfranck.cqrs.alemaire.subscription.command;

import eu.davidfranck.cqrs.alemaire.core.CommandHandler;
import eu.davidfranck.cqrs.alemaire.core.CommandResponse;
import eu.davidfranck.cqrs.alemaire.subscription.domain.ISubscriptionPlanRepository;
import eu.davidfranck.cqrs.alemaire.subscription.domain.SubscriptionPlan;
import eu.davidfranck.cqrs.alemaire.subscription.domain.SubscriptionPlanBuilder;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Component;

@Component
@Transactional // on peut gérer ça sans spring si on veut en démarrant la transaction dans un CommandBusMiddleware
public class CreateSubscriptionPlanCommandHandler implements CommandHandler<CreateSubscriptionPlanCommand> {

    private final ISubscriptionPlanRepository repository;

    public CreateSubscriptionPlanCommandHandler(ISubscriptionPlanRepository repository) {
        this.repository = repository;
    }

    @Override
    public Class<CreateSubscriptionPlanCommand> listenTo() {
        return CreateSubscriptionPlanCommand.class;
    }

    @Override
    public CommandResponse handle(CreateSubscriptionPlanCommand command) {
        SubscriptionPlan subscriptionPlan = new SubscriptionPlanBuilder(command.price(), command.name(), command.expirationDate())
                .withAnnualDiscount(command.annualDiscount())
                .withIncludedActivities(command.includedActivities())
                .build();
        Long id = repository.save(subscriptionPlan);
        return new CommandResponse(id);
    }
}
