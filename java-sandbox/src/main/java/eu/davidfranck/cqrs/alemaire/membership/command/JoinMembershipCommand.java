package eu.davidfranck.cqrs.alemaire.membership.command;

import eu.davidfranck.cqrs.alemaire.core.Command;

public record JoinMembershipCommand(String firstname, String lastname) implements Command {
}
