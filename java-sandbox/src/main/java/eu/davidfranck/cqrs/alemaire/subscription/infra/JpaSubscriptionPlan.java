package eu.davidfranck.cqrs.alemaire.subscription.infra;

import eu.davidfranck.cqrs.alemaire.activity.domain.Activity;
import eu.davidfranck.cqrs.alemaire.subscription.domain.SubscriptionPlan;
import eu.davidfranck.cqrs.alemaire.subscription.domain.SubscriptionPlanBuilder;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.List;

@Entity
public class JpaSubscriptionPlan {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private Long price;
    @Column
    private String name;
    @Column
    private LocalDate expirationDate;
    @Column
    private Long annualDiscount;
    @ManyToMany
    List<Activity> includedActivities;

    // pour hibernate
    public JpaSubscriptionPlan() {
    }

    public JpaSubscriptionPlan(SubscriptionPlan domain) {
        this.id = domain.getId();
        this.name = domain.getName();
        this.annualDiscount = domain.getAnnualDiscount();
        this.expirationDate = domain.getExpirationDate();
        this.includedActivities = domain.getIncludedActivities().stream()
                .map(Activity::new)
                .toList();
    }


    public Long getId() {
        return this.id;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public SubscriptionPlan toEntity() {
        List<Long> activities = includedActivities.stream().map(value -> getId()).toList();
        return new SubscriptionPlanBuilder(price, name, expirationDate)
                .withAnnualDiscount(annualDiscount)
                .withIncludedActivities(activities)
                .build();
    }
}
