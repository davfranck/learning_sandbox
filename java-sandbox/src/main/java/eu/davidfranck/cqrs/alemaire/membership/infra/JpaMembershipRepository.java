package eu.davidfranck.cqrs.alemaire.membership.infra;

import eu.davidfranck.cqrs.alemaire.membership.domain.Membership;
import eu.davidfranck.cqrs.alemaire.membership.domain.MembershipRepository;
import org.springframework.stereotype.Repository;

@Repository
public class JpaMembershipRepository implements MembershipRepository {

    private final IJpaMembershipRepository repository;

    public JpaMembershipRepository(IJpaMembershipRepository repository) {
        this.repository = repository;
    }

    @Override
    public Long save(Membership membership) {
        return repository.save(new JpaMembership(membership.getFirstname(), membership.getLastname())).getId();
    }
}
