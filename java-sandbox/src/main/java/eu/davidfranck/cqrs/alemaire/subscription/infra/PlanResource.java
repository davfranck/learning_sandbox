package eu.davidfranck.cqrs.alemaire.subscription.infra;

import eu.davidfranck.cqrs.alemaire.core.middleware.command.CommandBus;
import eu.davidfranck.cqrs.alemaire.subscription.command.CreateSubscriptionPlanCommand;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/plans")
public class PlanResource {
    private final CommandBus commandBus;

    public PlanResource(CommandBus commandBus) {
        this.commandBus = commandBus;
    }

    @PostMapping
    public ResponseEntity<Long> list(@RequestBody CreateSubscriptionPlanCommand command) {
        return ResponseEntity.ok().body(commandBus.dispatch(command).id());
    }
}
