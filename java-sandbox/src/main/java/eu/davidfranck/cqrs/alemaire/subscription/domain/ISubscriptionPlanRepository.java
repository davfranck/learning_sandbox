package eu.davidfranck.cqrs.alemaire.subscription.domain;

import java.util.List;

public interface ISubscriptionPlanRepository {
    Long save(SubscriptionPlan subscriptionPlan);

    List<SubscriptionPlan> findActivePlan();
}
