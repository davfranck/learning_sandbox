package eu.davidfranck.cqrs.alemaire.subscription.domain;

import eu.davidfranck.cqrs.alemaire.core.Event;

public record MemberJoined(Long newMemberId) implements Event {
}
