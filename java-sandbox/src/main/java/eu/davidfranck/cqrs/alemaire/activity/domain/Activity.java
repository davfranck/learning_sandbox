package eu.davidfranck.cqrs.alemaire.activity.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Activity {

    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;

    public Activity() {
    }

    public Activity(Long id) {
        this(id, null);
    }

    public Activity(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
