package eu.davidfranck.cqrs.alemaire.subscription.infra;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface SubscriptionJpaRepository extends JpaRepository<JpaSubscriptionPlan, Long> {

    List<JpaSubscriptionPlan> findAllByExpirationDate(LocalDate expirationDate);
}
