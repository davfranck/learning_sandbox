package eu.davidfranck.cqrs.alemaire.activity.infra;

import eu.davidfranck.cqrs.alemaire.activity.domain.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityJpaRepository extends JpaRepository<Activity, Long> {
}
