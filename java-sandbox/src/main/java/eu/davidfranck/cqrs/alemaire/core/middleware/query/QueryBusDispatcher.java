package eu.davidfranck.cqrs.alemaire.core.middleware.query;

import eu.davidfranck.cqrs.alemaire.core.Query;
import eu.davidfranck.cqrs.alemaire.core.QueryHandler;

import java.util.List;

public class QueryBusDispatcher<S, T extends Query<S>> implements QueryBusMiddleware<S, T> {
    private final List<QueryHandler<S, T>> queryHandlers;

    public QueryBusDispatcher(List<QueryHandler<S, T>> queryHandlers) {
        this.queryHandlers = queryHandlers;
    }

    @Override
    public S dispatch(T query) {
        return queryHandlers.stream()
                .filter(queryHandler -> queryHandler.listenTo().equals(query.getClass()))
                .findFirst()
                .orElseThrow()
                .handle(query);
    }
}
