package eu.davidfranck.cqrs.alemaire.subscription.query;

import eu.davidfranck.cqrs.alemaire.core.QueryHandler;
import eu.davidfranck.cqrs.alemaire.subscription.domain.ISubscriptionPlanRepository;

import java.util.List;

/**
 * Dans cette implémentation on est en CQS : on utilise le même repository que pour la commande.
 * On manipule donc aussi des entities du domaine métier ce que l'on va éviter en CQRS.
 */
public class FindActiveSubscriptionPlansQueryHandler implements QueryHandler<List<SubscriptionPlanViewModel>, FindActiveSubscriptionPlansQuery> {

    private final ISubscriptionPlanRepository subscriptionPlanRepository;

    public FindActiveSubscriptionPlansQueryHandler(ISubscriptionPlanRepository subscriptionPlanRepository) {
        this.subscriptionPlanRepository = subscriptionPlanRepository;
    }

    @Override
    public Class<FindActiveSubscriptionPlansQuery> listenTo() {
        return FindActiveSubscriptionPlansQuery.class;
    }

    @Override
    public List<SubscriptionPlanViewModel> handle(FindActiveSubscriptionPlansQuery query) {
        return subscriptionPlanRepository.findActivePlan()
                .stream()
                .map(SubscriptionPlanViewModel::new)
                .toList();
    }
}
