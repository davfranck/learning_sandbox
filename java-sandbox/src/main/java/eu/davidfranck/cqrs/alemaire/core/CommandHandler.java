package eu.davidfranck.cqrs.alemaire.core;

public interface CommandHandler<T extends Command> {

    Class<T> listenTo();

    CommandResponse handle(T command);
}
