package eu.davidfranck.cqrs.alemaire.core.middleware.command;

import eu.davidfranck.cqrs.alemaire.core.Command;
import eu.davidfranck.cqrs.alemaire.core.CommandHandler;
import eu.davidfranck.cqrs.alemaire.core.CommandResponse;

import java.util.List;

public class CommandBusDispatcher implements CommandBusMiddleware {

    private final List<CommandHandler> commandHandlers;

    public CommandBusDispatcher(List<CommandHandler> commandHandlers) {
        this.commandHandlers = commandHandlers;
    }

    @Override
    public CommandResponse dispatch(Command command) {
        return commandHandlers.stream()
                .filter(commandHandler -> commandHandler.listenTo().equals(command.getClass()))
                .findFirst()
                .orElseThrow()
                .handle(command);
    }
}
