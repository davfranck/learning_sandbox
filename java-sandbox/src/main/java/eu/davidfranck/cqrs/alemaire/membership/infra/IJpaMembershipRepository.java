package eu.davidfranck.cqrs.alemaire.membership.infra;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IJpaMembershipRepository extends JpaRepository<JpaMembership, Long> {
}
