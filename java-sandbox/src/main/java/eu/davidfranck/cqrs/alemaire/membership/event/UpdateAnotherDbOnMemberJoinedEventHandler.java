package eu.davidfranck.cqrs.alemaire.membership.event;

import eu.davidfranck.cqrs.alemaire.core.EventHandler;
import eu.davidfranck.cqrs.alemaire.subscription.domain.MemberJoined;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UpdateAnotherDbOnMemberJoinedEventHandler implements EventHandler<MemberJoined> {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateAnotherDbOnMemberJoinedEventHandler.class);
    @Override
    public Class<MemberJoined> listenTo() {
        return MemberJoined.class;
    }

    @Override
    public void handle(MemberJoined event) {
        LOGGER.info("update another db - projection");
    }
}
