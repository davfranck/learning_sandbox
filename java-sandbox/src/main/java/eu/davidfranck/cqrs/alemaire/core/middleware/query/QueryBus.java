package eu.davidfranck.cqrs.alemaire.core.middleware.query;

import eu.davidfranck.cqrs.alemaire.core.Query;
import eu.davidfranck.cqrs.alemaire.core.QueryHandler;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class QueryBus<R, T extends Query<R>> {

    private final QueryBusMiddleware<R, T> first;

    public QueryBus(List<QueryHandler<R,T>> queryHandlers) {
        this.first = new QueryBusDispatcher<>(queryHandlers);
    }

    public R dispatch(T query) {
        return first.dispatch(query);
    }
}
