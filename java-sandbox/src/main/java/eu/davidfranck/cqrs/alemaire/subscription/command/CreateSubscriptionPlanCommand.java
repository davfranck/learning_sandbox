package eu.davidfranck.cqrs.alemaire.subscription.command;

import eu.davidfranck.cqrs.alemaire.core.Command;

import java.time.LocalDate;
import java.util.List;

// Ceci est un DTO qui représente le paramètre d'un cas d'utilisation.
public record CreateSubscriptionPlanCommand(Long price, String name, LocalDate expirationDate, Long annualDiscount,
                                            List<Long> includedActivities) implements Command {
}
