package eu.davidfranck.cqrs.alemaire.membership.domain;

public interface MembershipRepository {
    Long save(Membership membership);
}
