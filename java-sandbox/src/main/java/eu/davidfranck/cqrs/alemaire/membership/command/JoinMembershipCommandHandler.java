package eu.davidfranck.cqrs.alemaire.membership.command;

import eu.davidfranck.cqrs.alemaire.core.CommandHandler;
import eu.davidfranck.cqrs.alemaire.core.CommandResponse;
import eu.davidfranck.cqrs.alemaire.membership.domain.Membership;
import eu.davidfranck.cqrs.alemaire.membership.domain.MembershipRepository;
import eu.davidfranck.cqrs.alemaire.subscription.domain.MemberJoined;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JoinMembershipCommandHandler implements CommandHandler<JoinMembershipCommand> {
    private final MembershipRepository membershipRepository;

    public JoinMembershipCommandHandler(MembershipRepository membershipRepository) {
        this.membershipRepository = membershipRepository;
    }

    @Override
    public Class<JoinMembershipCommand> listenTo() {
        return JoinMembershipCommand.class;
    }

    @Override
    public CommandResponse handle(JoinMembershipCommand command) {
        Long memberId = membershipRepository.save(new Membership(command.firstname(), command.lastname()));
        MemberJoined memberJoined = new MemberJoined(memberId);
        return new CommandResponse(memberId, List.of(memberJoined));
    }
}
