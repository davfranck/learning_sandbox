package eu.davidfranck.cqrs.alemaire.subscription.query;

import eu.davidfranck.cqrs.alemaire.subscription.domain.SubscriptionPlan;

public record SubscriptionPlanViewModel(Long id, String name) {
    public SubscriptionPlanViewModel(SubscriptionPlan subscriptionPlan) {
        this(subscriptionPlan.getId(), subscriptionPlan.getName());
    }
}
