package eu.davidfranck.cqrs.alemaire.core.middleware.command;

import eu.davidfranck.cqrs.alemaire.core.Command;
import eu.davidfranck.cqrs.alemaire.core.CommandResponse;

public interface CommandBusMiddleware {
    CommandResponse dispatch(Command command);
}
