package eu.davidfranck.cqrs.alemaire.core;

public interface QueryHandler<R, T extends Query<R>> {

    Class<T> listenTo();

    R handle(T query);
}
