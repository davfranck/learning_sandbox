package eu.davidfranck.cqrs.alemaire.subscription.domain;

import java.time.LocalDate;
import java.util.List;

public class SubscriptionPlanBuilder {
    private final Long price;
    private final String name;
    private final LocalDate expirationDate;
    private Long annualDiscount;
    private List<Long> includedActivities;

    public SubscriptionPlanBuilder(Long price, String name, LocalDate expirationDate) {
        this.price = price;
        this.name = name;
        this.expirationDate = expirationDate;
    }

    public SubscriptionPlanBuilder withAnnualDiscount(Long annualDiscount) {
        this.annualDiscount = annualDiscount;
        return this;
    }

    public SubscriptionPlanBuilder withIncludedActivities(List<Long> includedActivities) {
        this.includedActivities = includedActivities;
        return this;
    }

    public SubscriptionPlan build() {
        return new SubscriptionPlan(this);
    }

    public Long getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public Long getAnnualDiscount() {
        return annualDiscount;
    }

    public List<Long> getIncludedActivities() {
        return includedActivities;
    }
}