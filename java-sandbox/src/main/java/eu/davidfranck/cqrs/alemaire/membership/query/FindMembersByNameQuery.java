package eu.davidfranck.cqrs.alemaire.membership.query;

import eu.davidfranck.cqrs.alemaire.core.Query;
import eu.davidfranck.cqrs.alemaire.membership.infra.MemberView;

import java.util.List;

public record FindMembersByNameQuery(String name) implements Query<List<MemberView>> {
}
