package eu.davidfranck.cqrs.alemaire.core.middleware.command;

import eu.davidfranck.cqrs.alemaire.core.Command;
import eu.davidfranck.cqrs.alemaire.core.CommandHandler;
import eu.davidfranck.cqrs.alemaire.core.CommandResponse;
import eu.davidfranck.cqrs.alemaire.core.EventHandler;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CommandBus {

    private final CommandBusMiddleware first;

    public CommandBus(List<CommandHandler> commandHandlers, List<EventHandler> eventHandlers) {
        this.first =
                new LoggerMiddleware(
                        new EventBusDispatcher(
                                new CommandBusDispatcher(commandHandlers), eventHandlers));
    }

    public CommandResponse dispatch(Command command) {
        return this.first.dispatch(command);
    }
}
