package eu.davidfranck.cqrs.alemaire.subscription.query;

import eu.davidfranck.cqrs.alemaire.core.Query;

import java.util.List;

public record FindActiveSubscriptionPlansQuery() implements Query<List<SubscriptionPlanViewModel>> {
}
