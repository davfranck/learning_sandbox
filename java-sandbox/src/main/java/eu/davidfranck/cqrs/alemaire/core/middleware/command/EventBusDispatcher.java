package eu.davidfranck.cqrs.alemaire.core.middleware.command;

import eu.davidfranck.cqrs.alemaire.core.Command;
import eu.davidfranck.cqrs.alemaire.core.CommandResponse;
import eu.davidfranck.cqrs.alemaire.core.EventHandler;

import java.util.List;

public class EventBusDispatcher implements CommandBusMiddleware {
    private final CommandBusMiddleware next;
    private final List<EventHandler> eventHandlers;

    public EventBusDispatcher(CommandBusMiddleware next, List<EventHandler> eventHandlers) {
        this.next = next;
        this.eventHandlers = eventHandlers;
    }

    @Override
    public CommandResponse dispatch(Command command) {
        CommandResponse commandResponse = next.dispatch(command);
        if (commandResponse.hasEvent()) {
            commandResponse.events().forEach(event -> {
                eventHandlers.stream()
                        .filter(eventHandler -> eventHandler.listenTo().equals(event.getClass()))
                        .forEach(eventHandler -> eventHandler.handle(event));
            });
        }
        return commandResponse;
    }
}
