package eu.davidfranck.cqrs.alemaire.membership.infra;

import eu.davidfranck.cqrs.alemaire.core.CommandResponse;
import eu.davidfranck.cqrs.alemaire.core.middleware.command.CommandBus;
import eu.davidfranck.cqrs.alemaire.core.middleware.query.QueryBus;
import eu.davidfranck.cqrs.alemaire.membership.command.JoinMembershipCommand;
import eu.davidfranck.cqrs.alemaire.membership.query.FindMembersByNameQuery;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/memberships")
public class MembershipResource {
    private final CommandBus commandBus;
    private final QueryBus queryBus;

    public MembershipResource(CommandBus commandBus, QueryBus queryBus) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
    }

    @PostMapping
    public ResponseEntity<Long> newMembership(@RequestBody JoinMembershipCommand joinMembershipCommand) {
        CommandResponse commandResponse = this.commandBus.dispatch(joinMembershipCommand);
        return ResponseEntity.ok(commandResponse.id());
    }

    @GetMapping
    public ResponseEntity<List<MemberView>> findMembershipsByName(@RequestBody FindMembersByNameQuery query) {
        return ResponseEntity.ok((List<MemberView>) queryBus.dispatch(query));
    }
}
