package eu.davidfranck.cqrs.alemaire.membership.query;

import eu.davidfranck.cqrs.alemaire.core.QueryHandler;
import eu.davidfranck.cqrs.alemaire.membership.infra.MemberView;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Ce query handler lui est indépendant du repository de la partie command.
 * On a donc une séparation de la responsabilité au niveau persistance => CQRS.
 */
@Component
public class FindMembersByNameQueryHandler implements QueryHandler<List<MemberView>, FindMembersByNameQuery> {
    private final JdbcTemplate jdbcTemplate;

    public FindMembersByNameQueryHandler(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Class<FindMembersByNameQuery> listenTo() {
        return FindMembersByNameQuery.class;
    }

    @Override
    public List<MemberView> handle(FindMembersByNameQuery query) {
        return jdbcTemplate.query("select id, firstname, lastname from Jpa_Membership where lastname like ?", new RowMapper<MemberView>() {
            @Override
            public MemberView mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new MemberView(rs.getLong("id"), rs.getString("firstname"), rs.getString("lastname"));
            }
        }, "%" + query.name() + "%");
    }
}
