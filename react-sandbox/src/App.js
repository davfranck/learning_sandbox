import "./App.css";
import { Route, Switch } from "react-router-dom";
import Welcome from "./pages/Welcome";
import Products from "./pages/Products";
import MainHeader from "./components/MainHeader";
import ProductDetail from "./pages/ProductDetail";
import HookTest from "./components/HookTest";

function App() {
    return (
        <div>
            <MainHeader />
            <main>
                <h1>React</h1>
                <h2>Router</h2>
                <p>
                    On utilise <code>Switch</code> pour indiquer que l'on souhaite afficher uniquement la première <code>Route</code> qui match. 
                    On peut utiliser aussi <code>&lt;Route path="..." exact&gt;</code>
                </p>
                <Switch>
                    <Route path="/welcome">
                        <Welcome />
                    </Route>
                    <Route path="/products/:productId">
                        <ProductDetail />
                    </Route>
                    <Route path="/products">
                        <Products />
                    </Route>
                </Switch>
                <HookTest></HookTest>
            </main>
        </div>
    );
}

export default App;
