import { useEffect, useState } from "react"

const useCallFakeData = () => {
    const [ fakeData, setFakeData ] = useState()
    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/posts/1')
            .then(response => response.json())
            .then(json => setFakeData(json))
    }, [])
    console.log("fakeData : " + fakeData)
    return fakeData
}

export default useCallFakeData