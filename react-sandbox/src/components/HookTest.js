import useCallFakeData from '../hooks/use-call-fake-data'

const HookTest = () => {
    const fakeData = useCallFakeData()
    console.log(fakeData)
    return <div>
        <p>Résultat du hook 'useCallFakeData'</p>   
        {fakeData && 
        <ul>
            <li>{fakeData.userId}</li>
            <li>{fakeData.id}</li>
            <li>{fakeData.title}</li>
        </ul>
        }
    </div>
}

export default HookTest