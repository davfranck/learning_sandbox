import { Link } from "react-router-dom";

const Products = () => {
    return (
        <>
            <h1>The Products Page</h1>
            <ul>
                <li><Link to='/products/1'>Vers produit 1</Link></li>
                <li><Link to='/products/2'>Vers produit 2</Link></li>
            </ul>
        </>
    );
};

export default Products;
