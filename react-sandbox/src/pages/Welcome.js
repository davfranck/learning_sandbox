import { Route, Link } from "react-router-dom";

const Welcome = () => {
    return <>
        <h1>The Welcome Page</h1>
        <p>La nested Route s'affiche que si on est sur l'url /welcome/new-user</p>
        <Link to='/welcome/new-user'>Vers la nested route</Link>
        <Route path="/welcome/new-user">
            <h2>Nested Route !</h2>
            <p>Welcome to new user</p>
        </Route>
    </>
}

export default Welcome