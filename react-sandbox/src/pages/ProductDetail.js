import { useParams } from "react-router-dom"

const ProductDetail = props => {
    const params = useParams();
    return <h1>Product detail {params.productId}</h1>
}

export default ProductDetail