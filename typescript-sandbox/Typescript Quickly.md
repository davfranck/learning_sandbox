# Typescript Quickly
Note du livre. 

Playground : http://www.typescriptlang.org/play

## Mise en place
J'ai installé NVM préalablement. 

```shell
$ npm init --yes
$ tsc init 
$ npm i jest @types/jest ts-jest typescript -D
```
Note : `-D` est équivalent à `--save-dev`, donc typescript est ajouté aux dépendances du projet uniquement. 

Création d'un fichier jest.config.js : 
```
module.exports = {
    "roots": [
        // tous les fichiers typescript sont dans src
        "<rootDir>/src"
    ],
    // patterns pour trouver les fichiers de test
    "testMatch": [
        "**/__tests__/**/*.+(ts|tsx|js)",
        "**/?(*.)+(spec|test).+(ts|tsx|js)"
    ],
    // pour la tranformation des fichiers ts afin de pouvoir les exécuter
    "transform": {
        "^.+\\.(ts|tsx)$": "ts-jest"
    },
}
```
### Exécution
Ts => Js : `$ tsc`. 
On peut passer des options à `tsc` ou les mettre dans un fichier tsconfig.json : 

```            
"target": "es2018",   // version cible de transpilation                
"rootDir": "./src",
"outDir": "./dist", 
"watch": true // tsc va compiler automatiquement tout ce qui sera modifié dans ce dossier, la commande ne rend donc pas la main
"noEmitOnError": true, // les fichiers JS ne seront pas générés tant que l'on n'a pas résolu les erreurs
```

Lancement des tests : `npm t`. 

## Typescript et Javascript
Le code Typescript est transpilé (le mot compilé est largement utilisé, à tort) vers du Javascript. 
On peut transpilé vers la version de JS de notre choix. 
Note : on ne pourra pas aller vers toutes les versions si je comprends bien (par exemple utiliser des getters et aller vers du js3). 
Typescript embarque très tôt les nouvelles features de JS (parfois avant même que les navigateurs les proposent). 

Worflow typique :
1. transpiler chaque fichier TS vers du JS dans la version choisie
2. bundle (créer un paquet) en un seul fichier JS

On peut utiliser de libs JS, mais on n'aura alors pas d'aide de notre ide (autocomplétion, messages d'erreur). 
En revanche il y a des définitions de type (fichier en `.d.ts`) qui permettent d'ajouter des types pour les librairies écrites en Javascript. 

![Workflow](images/ts_workflow.jpg)
*Source : Typescript Quickly*

## Node
Node c'est un framework mais aussi un *runtime* de Javascript. 
Il permet d'exécuter du JS en dehors d'un navigateur, de lancer des outils comme NPM. 

## VS Code
Libs conseillées : 
* ESLint pour vérifier la maintenabilité et lisibilité
* Prettier pour le formattage

WIP : https://learning.oreilly.com/library/view/typescript-quickly/9781617295942/kindle_split_010.html

## Types

On peut utiliser les types de Javascript. 

Il y a aussi `any` qui peut représenter n'importe quel type. 
Mais en général lui préférer l'union de types. 
Ex : `let myVar:string | number`. 

On peut déclarer nos types avec `type` : 

```typescript
type Centimetre = number;
type Celsius = number;
type Patient = {
    nom: string;
    taille: Centimetre;
    temperature?: Celsius; // ? signifie que le champ est optionnel
}
let john:Patient = {nom:"john doe", taille: 123, temperature: 456}
```

Le code ci-dessus ne génère que le js suivant : 

```javascript
let john = { nom: "john doe", taille: 123, temperature: 456 };
```

Idem si on utilise une interface : 

```typescript
interface Patient {
    nom:string;
    taille:number;
    temperature:number;
}
let john:Patient = {nom:"john doe", taille: 123, temperature: 456}
```
Génère le même JS que pour `type`. 

En revanche pour une classe c'est différent : 

```typescript
class Patient {
    constructor(readonly nom:string, public taille:number, public temperature:number) {}
}
```

génère le JS suivant : 

```javascript
class Patient {
    constructor(nom, taille, temperature) {
        this.nom = nom;
        this.taille = taille;
        this.temperature = temperature;
    }
}
let john = { nom: "john doe", taille: 123, temperature: 456 };
```

On utilise `class` si on cherche à représenter une valeur. 
On pourra utiliser dessus `typeof` ou `instanceof`. 
En revanche on pourra utiliser `type` ou  `interface` si on ne souhaite que bénéficier de la sécurité des types. 
L'empreinte JS est plus petite dans ces deux cas, puisqu'aucun code n'est généré. 

### Structural type system

En typescript le code suivant est ok : 

```typescript
test("structural type system : Patient, Patient2 et Person sont équivalents", () => {
    let johnDoe:Person = new Patient2("JDoe", 123, 456);
    let janeDoe:Patient = new Patient2("Jane", 567, 987);
    expect(johnDoe.nom).toEqual("JDoe");
    expect(janeDoe.nom).toEqual("Jane");
});
```

## TODOs - A creuser
* creuser l'usage des unions de type

https://learning.oreilly.com/library/view/typescript-quickly/9781617295942/kindle_split_011.html