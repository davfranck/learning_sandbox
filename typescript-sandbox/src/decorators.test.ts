/*
Il faut activer experimentalDecorators dans tsconfig.js
*/

// target représente le constructeur de la classe annotée.
function whoAmI(target: Function) {
    console.log(`you are ${target}`);
}

@whoAmI
class Friend {
    constructor(public name:string, private age:number){}
}

test("decorators", () => {
    new Friend("bob", 12);
});

function UIComponent(html:string) {
    console.log(`received : ${html}`);
    return function(target:Function) {
        console.log(`used from ${target} and still access to ${html}`);
    }
}

@UIComponent("<h1>$placeholder</h1>")
class Foo {
    constructor(public name:string){}
}

test("décorators 2", () => {
    new Foo("name");
});