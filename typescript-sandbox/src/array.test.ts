test("spread oprator", () => {
    let input:string[] = ["one", "two", "three"];
    expect(["0", ...input, "4"]).toEqual(["0", "one", "two", "three", "4"]);
});

function rest(...someArgs:any[]):any[] {
    return someArgs;
}

test("rest parameter", () => {
    expect(rest(1,2,3)).toEqual([1,2,3]);
    expect(rest("1","2","3")).toEqual(["1","2","3"]);
});

////////////////////////////////////////////////////////////////////////////////
// Array destructuring
////////////////////////////////////////////////////////////////////////////////
test("array destructuring", () => {
    let tab = ["a", "b"];
    let [a,b] = tab;
    expect(a).toBe("a");
    expect(b).toBe("b");
    // ça équivaut à 
    // a = tab[0]
    // b = tab[1]
});

test("array destructuring with rest", () => {
    let tab = ["a", "b", "c"];
    let [a, ...rest] = tab;
    expect(a).toBe("a");
    expect(rest).toEqual(["b", "c"]);
});

test("array destructuring and ignore rest", () => {
    let tab = ["a", "b", "c"];
    let [a] = tab;
    expect(a).toBe("a");
});