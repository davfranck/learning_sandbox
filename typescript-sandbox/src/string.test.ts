test("template literal", () => {
    let expression = "b"
    let sample = `a ${expression} c`
    expect(sample).toBe("a b c");
});

test("template literal multiline", () => {
    let expression = "b"
    let sample = `a ${expression} c
    d`
    expect(sample).toBe("a b c\n    d");
});