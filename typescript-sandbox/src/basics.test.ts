test("null", () => {
    let myVar = null;
    expect(myVar).toBeNull();
});

test("undefined", () => {
    let myVar;
    expect(myVar).toBeUndefined();
});

// typeof est utilisé avec les types de javascript
// instanceof est utilisé avec les types custom

////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function unionWithUndefined(input:string): string | undefined {
    if(input == "foo") {
        return "toto";
    }
    // pas de else => retourne undefined dans les autres cas
}

test("function with undefined", () => {
    expect(unionWithUndefined("foo")).toEqual("toto");
    expect(unionWithUndefined("unknown")).toBeUndefined();
});

function withFuntionParameter(callback: (el:string) => string, name:string):string {
    return callback(name);
}

test("callback", () => {
    expect(withFuntionParameter((item) => "Hello " + item, "World")).toEqual("Hello World");
});