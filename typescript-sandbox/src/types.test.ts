////////////////////////////////////////////////////////////////////////////////////////
// mot clé `type`
////////////////////////////////////////////////////////////////////////////////////////

type Centimetre = number;
type Celsius = number;
type Patient = {
    nom: string;
    taille: Centimetre;
    temperature?: Celsius; // ? signifie que le champ est optionnel
}

test("types", () => {
    let johnDoe:Patient = { nom: "JDoe", taille: 180 };
    expect(johnDoe).not.toBeNull();
});

////////////////////////////////////////////////////////////////////////////////////////
// classes
////////////////////////////////////////////////////////////////////////////////////////

class Patient2 {
    static TOTAL:number = 0;
    // "nom" n'est accessible qu'en lecture avec "readonly"
    // private n'est pas accesible
    constructor(readonly nom:string, public taille:Centimetre, public temperature:Celsius) {
        Patient2.TOTAL++;
    }

    sayHello():string {
        return "Hello";
    }
}

test("class", () => {
    expect(new Patient2("JDoe", 123, 456).nom).toEqual("JDoe");

    // ci dessous ne compile pas car il n'y a pas la fonction sayHello dans l'object literal
    // let calimero:Patient2 = { nom: "calimero", taille: 3, temperature: 45 };
});

// avec un constructeur "classique" (sans affectation auto des membres)
class Ordinateur {
    public marque:string;
    private modele:string;
    constructor(marque:string, modele:string) {
        this.marque = marque;
        this.modele = modele;
    }
}

test('constructeur classique', () => {
    expect(new Ordinateur("dell", "e45").marque).toEqual("dell");
})
////////////////////////////////////////////////////////////////////////////////////////
// interfaces
////////////////////////////////////////////////////////////////////////////////////////

interface Person {
    nom:string;
    taille:Centimetre;
    temperature:Celsius;
}

test("interface", () => {
    let johnDoe:Person = { nom:"JDoe", taille: 123, temperature: 38 };
    expect(johnDoe.nom).toEqual("JDoe");
});

test("structural type system : Patient, Patient2 et Person sont équivalents", () => {
    let johnDoe:Person = new Patient2("JDoe", 123, 456);
    expect(johnDoe.nom).toEqual("JDoe");

    let janeDoe:Patient = new Patient2("Jane", 567, 987);
    expect(janeDoe.nom).toEqual("Jane");

    let objectLiteral:Person = { nom: "Roger", temperature: 234, taille: 567 };
    expect(objectLiteral.nom).toEqual("Roger");
});

////////////////////////////////////////////////////////////////////////////////////////
// abstract + héritage
////////////////////////////////////////////////////////////////////////////////////////

abstract class Human {
    constructor(public name:string){};

    abstract sayHello():string;
    foo():string {
        return "foo";
    }
}
class Employee extends Human {
    // pas de modifier devant name qui est défini dans la classe parente
    constructor(name:string, public matricule:string){
        super(name);
    }
    sayHello(): string {
        return "Hello !";
    }
    // redéfinition
    foo():string {
        return "bar";
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// surcharge 
////////////////////////////////////////////////////////////////////////////////////////

class Product {
    id: number;
    description: string;

    constructor();                                     
    constructor(id: number);                           
    constructor(id: number, description: string);      
    constructor(id?: number, description?: string) {
        this.id = id == undefined ? 1 : id;
        this.description = description == undefined ? "default" : description;
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// union de types
////////////////////////////////////////////////////////////////////////////////////////

interface Alain { delon:string }
interface Catherine { deneuve: string}
function alien(person: Alain | Catherine): string {
    if("delon" in person) {
        return "de loin";
    }
    return "toute neuve"
}

test("union de types", () => {
    expect(alien({delon: "alain delon"})).toEqual("de loin");
    expect(alien({deneuve: "alain delon"})).toEqual("toute neuve");
});

// UNION - avec discriminant (ici : kind est commun et permet de différencié à qui on a à faire)
interface Rectangle {
    kind: "rectangle";
    width: number;
    height: number;
}
interface Circle {
    kind: "circle"; 
    radius: number;
}
 
type Shape = Rectangle | Circle;

function area(shape: Shape): number {
    switch (shape.kind) {
        case "rectangle": return shape.height * shape.width;
        case "circle": return Math.PI * shape.radius ** 2;
    }
}

test("union avec discriminant", () => {
    const myCircle: Circle = { kind: "circle", radius: 10};
    expect(area(myCircle)).toEqual(314.1592653589793);
});

////////////////////////////////////////////////////////////////////////////////////////
// ENUMS
////////////////////////////////////////////////////////////////////////////////////////

enum Weekdays {
    Monday = 1, // par défaut c'est zero si on ne change pas
    Tuesday = 2, // cette valeur et les suivantes auraient pu être ignorées, car valorisée à 2 automatiquement
    Wednesday = 3,
    Thursday = 4,
    Friday = 5,
    Saturday = 6,
    Sunday = 7
}
test("enums numériques", () => {
    expect(Weekdays.Wednesday).toBe(3);
    expect(Weekdays[3]).toBe("Wednesday");
});

// string enum
enum Direction {
    Up = "UP",
    Down = "DOWN",
    Left = "LEFT",
    Right = "RIGHT",
}

test("enum string", () => {
    expect(Direction.Up).toEqual("UP");
});

////////////////////////////////////////////////////////////////////////////////////////
// GENERICS
////////////////////////////////////////////////////////////////////////////////////////
// déclarations équivalentes : 
const someValues: number[] = [];
const someValues2: Array<number> = [];